using SqlTrainer.DatabaseInfoService.Application.Extensions;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Extensions;
using SqlTrainer.DatabaseInfoService.Endpoints;
using SqlTrainer.DatabaseInfoService.Extensions;
using SqlTrainer.DatabaseInfoService.Persistence.Extensions;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpClient();

builder.Services.AddDatabaseServices(builder.Configuration);

builder.Services
    .AddPersistence(builder.Configuration)
    .AddApplication(builder.Configuration);

var app = builder.Build();

app.UseHttpsRedirection();

app.MapLanguageEndpoints();
app.MapDatabaseEndpoints();
app.MapTableEndpoints();

app.Migrate().Run();