using System.Text.Json.Serialization;
using SqlTrainer.DatabaseInfoService.Application.Databases;

namespace SqlTrainer.DatabaseInfoService.Requests;

public sealed class PostPutDatabaseRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("languageId")]
    public Guid? LanguageId { get; init; }
    
    public AddDatabaseRequest ToAddMediatorRequest()
    {
        if (!LanguageId.HasValue)
            throw new ArgumentException($"{nameof(LanguageId)} is required for {nameof(AddDatabaseRequest)}.");
                
        return new AddDatabaseRequest(Name, LanguageId.Value);
    }
    
    public UpdateDatabaseRequest ToUpdateMediatorRequest(Guid databaseId)
    {
        return new UpdateDatabaseRequest(databaseId, Name);
    }
}