using System.Text.Json.Serialization;
using SqlTrainer.DatabaseInfoService.Application.Tables;

namespace SqlTrainer.DatabaseInfoService.Requests;

public sealed class PostPutTableRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("databaseId")]
    public Guid? DatabaseId { get; init; }

    [JsonPropertyName("attributes")]
    public IReadOnlyCollection<PostPutTableAttributeRequest> Attributes { get; init; } =
        Array.Empty<PostPutTableAttributeRequest>();

    public AddTableRequest ToAddMediatorRequest()
    {
        if (!DatabaseId.HasValue)
            throw new ArgumentException($"{nameof(DatabaseId)} is required for {nameof(AddTableRequest)}.");
        
        return new AddTableRequest(Name, DatabaseId.Value, Attributes.Select(a => a.ToAddMediatorRequest()));
    }
    
    public UpdateTableRequest ToUpdateMediatorRequest(Guid tableId)
    {
        return new UpdateTableRequest(tableId, Name, Attributes.Select(a => a.ToUpdateMediatorRequest(tableId)).ToArray());
    }
}

public sealed class PostPutTableAttributeRequest
{
    [JsonPropertyName("id")]
    public Guid? Id { get; init; }
    
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("type")]
    public required string Type { get; init; }
    
    [JsonPropertyName("isPrimaryKey")]
    public required bool IsPrimaryKey { get; init; }
    
    [JsonPropertyName("isNotNull")]
    public required bool IsNotNull { get; init; }
    
    [JsonPropertyName("isUnique")]
    public required bool IsUnique { get; init; }
    
    [JsonPropertyName("defaultValue")]
    public string? DefaultValue { get; init; }
    
    [JsonPropertyName("varcharNumberOfSymbols")]
    public int? VarcharNumberOfSymbols { get; init; }
    
    [JsonPropertyName("foreignKeyId")]
    public Guid? ForeignKeyId { get; init; }
    
    [JsonPropertyName("order")]
    public required int Order { get; init; }
    
    public AddTableAttributeRequest ToAddMediatorRequest()
    {
        return new AddTableAttributeRequest(Name, Type, IsPrimaryKey, IsNotNull, IsUnique, DefaultValue, VarcharNumberOfSymbols, ForeignKeyId, Order);
    }
    
    public UpdateTableAttributeRequest ToUpdateMediatorRequest(Guid tableId)
    {
        return new UpdateTableAttributeRequest(Id, tableId, Name, Type, Order, IsPrimaryKey, IsNotNull, IsUnique, DefaultValue, VarcharNumberOfSymbols, ForeignKeyId);
    }
}
