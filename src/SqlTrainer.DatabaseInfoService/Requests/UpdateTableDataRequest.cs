using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.Requests;

public sealed class UpdateTableDataRequest
{
    [JsonPropertyName("old")]
    public required JsonObject OldData { get; init; }
    
    [JsonIgnore]
    public string OldDataJson => OldData.ToJsonString();
    
    [JsonPropertyName("new")]
    public required JsonObject NewData { get; init; }
    
    [JsonIgnore]
    public string NewDataJson => NewData.ToJsonString();
}