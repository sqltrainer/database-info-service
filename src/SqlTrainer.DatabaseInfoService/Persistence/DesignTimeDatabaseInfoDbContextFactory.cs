using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using SqlTrainer.DatabaseInfoService.Persistence.Data;
using SqlTrainer.DatabaseInfoService.Persistence.Extensions;

namespace SqlTrainer.DatabaseInfoService.Persistence;

// ReSharper disable once UnusedType.Global
/// <summary>
/// The design time database context factory for the database info database context.
/// </summary>
/// <remarks>
/// It's used to create a database context for design time tools like EF Core migrations.
/// </remarks>
public sealed class DesignTimeDatabaseInfoDbContextFactory : IDesignTimeDbContextFactory<DatabaseInfoDbContext>
{
    public DatabaseInfoDbContext CreateDbContext(string[] args)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddUserSecrets(Assembly.GetExecutingAssembly())
            .AddEnvironmentVariables()
            .Build();
        
        var builder = new DbContextOptionsBuilder<DatabaseInfoDbContext>();

        builder.UseDatabaseInfoDb(configuration);

        return new DatabaseInfoDbContext(builder.Options);
    }
}