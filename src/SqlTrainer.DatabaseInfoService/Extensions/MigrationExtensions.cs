using Microsoft.EntityFrameworkCore;
using SqlTrainer.DatabaseInfoService.Persistence.Data;

namespace SqlTrainer.DatabaseInfoService.Extensions;

public static class MigrationExtensions
{
    public static IHost Migrate(this IHost host)
    {
        using var scope = host.Services.CreateScope();
        var services = scope.ServiceProvider;
        
        var loggerFactory = services.GetRequiredService<ILoggerFactory>();
        var logger = loggerFactory.CreateLogger<IHost>();

        try
        {
            var dbContext = services.GetRequiredService<DatabaseInfoDbContext>();
            dbContext.Database.Migrate();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while migrating the database.");
        }

        return host;
    }
}