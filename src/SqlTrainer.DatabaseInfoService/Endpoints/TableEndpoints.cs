using System.Text.Json.Nodes;
using DatabaseHelper.ScriptResults;
using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseInfoService.Application.Attributes;
using SqlTrainer.DatabaseInfoService.Application.Tables;
using SqlTrainer.DatabaseInfoService.Application.Tables.Data;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.Requests;

namespace SqlTrainer.DatabaseInfoService.Endpoints;

public static class TableEndpoints
{
    public static void MapTableEndpoints(this IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("tables");
        
        group.MapPost("/", CreateTableAsync)
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("createTable")
            .WithDisplayName("Create Table");
        
        group.MapDelete("/{id:guid}", DropTableAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("dropTable")
            .WithDisplayName("Drop Table");
        
        group.MapPut("/{id:guid}", AlterTableAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("alterTable")
            .WithDisplayName("Alter Table");

        group.MapGet("/{id:guid}", GetTableByIdAsync)
            .Produces<Table>()
            .Produces(StatusCodes.Status404NotFound)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getTableById")
            .WithDisplayName("Get Table by Id");
        
        group.MapGet("/", GetAllTablesAsync)
            .Produces<IReadOnlyCollection<Table>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getAllTests")
            .WithDisplayName("Get All Tests")
            .WithDescription("Get all tests");
        
        group.MapGet("/{tableId:guid}/attributes", GetTableAttributesAsync)
            .Produces<IReadOnlyCollection<Domain.Models.Attribute>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getTableAttributes")
            .WithDisplayName("Get Table Attributes");
        
        var dataGroup = group.MapGroup("/{tableId:guid}/data");
        
        dataGroup.MapPost("/insert", InsertTableDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("insertTableData")
            .WithDisplayName("Insert Data Into Table");
        
        dataGroup.MapPost("/update", UpdateTableDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("updateTableData")
            .WithDisplayName("Update Data In Table");
        
        dataGroup.MapPost("/delete", DeleteTableDataAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("deleteTableData")
            .WithDisplayName("Delete Data From Table");
        
        dataGroup.MapGet("/", SelectTableDataAsync)
            .Produces<ScriptResult>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("selectTableData")
            .WithDisplayName("Select Data From Table");
    }

    private static async Task<IResult> CreateTableAsync(
        HttpContext context,
        [FromServices] IAddTableRequestHandler handler,
        [FromBody] PostPutTableRequest request,
        CancellationToken cancellationToken)
    {
        try
        {
            var mediatorRequest = request.ToAddMediatorRequest();
        
            var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
            return result.IsSucceeded
                ? Microsoft.AspNetCore.Http.Results.Created($"{context.Request.Path.Value}/{result.Value}", result.Value)
                : result.ToAspNetResult();
        }
        catch (ArgumentException ex)
        {
            return Microsoft.AspNetCore.Http.Results.BadRequest(ex.Message);
        }
    }
    
    private static async Task<IResult> DropTableAsync(
        [FromServices] IDeleteTableRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteTableRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> AlterTableAsync(
        [FromServices] IUpdateTableRequestHandler handler,
        [FromRoute] Guid id,
        [FromBody] PostPutTableRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = request.ToUpdateMediatorRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetTableByIdAsync(
        [FromServices] IGetTableByIdRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetTableByIdRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllTablesAsync(
        [FromServices] IGetAllTablesRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllTablesRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> GetTableAttributesAsync(
        [FromServices] IGetAttributesByTableRequestHandler handler,
        [FromRoute] Guid tableId,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAttributesByTableRequest(tableId);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> InsertTableDataAsync(
        [FromServices] IInsertDataRequestHandler handler,
        [FromRoute] Guid tableId,
        [FromBody] JsonObject request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new InsertDataRequest(tableId, request.ToJsonString());
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> UpdateTableDataAsync(
        [FromServices] IUpdateDataRequestHandler handler,
        [FromRoute] Guid tableId,
        [FromBody] UpdateTableDataRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new UpdateDataRequest(tableId, request.OldDataJson, request.NewDataJson);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> DeleteTableDataAsync(
        [FromServices] IDeleteDataRequestHandler handler,
        [FromRoute] Guid tableId,
        [FromBody] JsonObject request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteDataRequest(tableId, request.ToJsonString());
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> SelectTableDataAsync(
        [FromServices] ISelectDataRequestHandler handler,
        [FromRoute] Guid tableId,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new SelectDataRequest(tableId);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
}