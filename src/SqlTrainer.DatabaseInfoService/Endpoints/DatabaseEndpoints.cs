using DatabaseHelper.ScriptResults;
using HttpServer.Requests;
using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseInfoService.Application.Attributes;
using SqlTrainer.DatabaseInfoService.Application.Databases;
using SqlTrainer.DatabaseInfoService.Application.Scripts;
using SqlTrainer.DatabaseInfoService.Application.Tables;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.Requests;

namespace SqlTrainer.DatabaseInfoService.Endpoints;

public static class DatabaseEndpoints
{
    public static void MapDatabaseEndpoints(this IEndpointRouteBuilder app)
    {
        var group = app.MapGroup("databases");
        
        group.MapPost("/", CreateDatabaseAsync)
            .Produces<Guid>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("createDatabase")
            .WithDisplayName("Create Database");
        
        group.MapDelete("/{id:guid}", DropDatabaseAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("dropDatabase")
            .WithDisplayName("Drop Database");
        
        group.MapPut("/{id:guid}", RenameDatabaseAsync)
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("renameDatabase")
            .WithDisplayName("Rename Database");
        
        group.MapGet("/{id:guid}", GetDatabaseByIdAsync)
            .Produces<Database>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getDatabaseById")
            .WithDisplayName("Get Database by Id");
        
        group.MapGet("/", GetAllDatabasesAsync)
            .Produces<IReadOnlyCollection<Database>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getAllDatabases")
            .WithDisplayName("Get All Databases");
        
        group.MapGet("/{databaseId:guid}/tables", GetDatabaseTablesAsync)
            .Produces<IReadOnlyCollection<Table>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getDatabaseTables")
            .WithDisplayName("Get Tables by Database Id");
        
        group.MapGet("/{databaseId:guid}/primary-keys", GetPrimaryKeysAsync)
            .Produces<IReadOnlyCollection<Domain.Models.Attribute>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getPrimaryKeys")
            .WithDisplayName("Get all primary keys for a database");
        
        var scriptsGroup = group.MapGroup("{databaseId:guid}/scripts");
        
        scriptsGroup.MapPost("/", ExecuteAsync)
            .Produces<ScriptResult>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("executeScript")
            .WithDisplayName("Execute a script");
        
        scriptsGroup.MapPost("/batch", ExecuteBatchAsync)
            .Produces<IReadOnlyCollection<ScriptResult>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("executeScriptBatch")
            .WithDisplayName("Execute a batch of scripts");
    }

    private static async Task<IResult> CreateDatabaseAsync(
        HttpContext context,
        [FromServices] IAddDatabaseRequestHandler handler,
        [FromBody] PostPutDatabaseRequest request,
        CancellationToken cancellationToken)
    {
        try
        {
            var mediatorRequest = request.ToAddMediatorRequest();

            var result = await handler.HandleAsync(mediatorRequest, cancellationToken);

            return result.IsSucceeded
                ? Microsoft.AspNetCore.Http.Results.Created($"{context.Request.Path.Value}/{result.Value}", result.Value)
                : result.ToAspNetResult();
        }
        catch (ArgumentException ex)
        {
            return Microsoft.AspNetCore.Http.Results.BadRequest(ex.Message);
        }
    }
    
    private static async Task<IResult> DropDatabaseAsync(
        [FromServices] IDeleteDatabaseRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new DeleteDatabaseRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> RenameDatabaseAsync(
        [FromServices] IUpdateDatabaseRequestHandler handler,
        [FromRoute] Guid id,
        [FromBody] PostPutDatabaseRequest request,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = request.ToUpdateMediatorRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetDatabaseByIdAsync(
        [FromServices] IGetDatabaseByIdRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetDatabaseByIdRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetAllDatabasesAsync(
        [FromServices] IGetAllDatabasesRequestHandler handler,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllDatabasesRequest(query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetDatabaseTablesAsync(
        [FromServices] IGetTablesByDatabaseRequestHandler handler,
        [FromRoute] Guid databaseId,
        FilterPaginatedQuery query,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetTablesByDatabaseRequest(databaseId, query.Search, query.SortBy, query.SortDirection, query.Page, query.PageSize);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> GetPrimaryKeysAsync(
        [FromServices] IGetPrimaryKeysByDatabaseRequestHandler handler,
        [FromRoute] Guid databaseId,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetPrimaryKeysByDatabaseRequest(databaseId);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> ExecuteAsync(
        [FromServices] IExecuteScriptRequestHandler handler,
        [FromRoute] Guid databaseId,
        [FromBody] string script,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new ExecuteScriptRequest(databaseId, script);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }

    private static async Task<IResult> ExecuteBatchAsync(
        [FromServices] IExecuteScriptsRequestHandler handler,
        [FromRoute] Guid databaseId,
        [FromBody] IReadOnlyCollection<string> scripts,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new ExecuteScriptsRequest(databaseId, scripts);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
}