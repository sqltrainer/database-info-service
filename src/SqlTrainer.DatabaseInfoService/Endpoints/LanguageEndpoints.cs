using Microsoft.AspNetCore.Mvc;
using Results.WebExtensions;
using SqlTrainer.DatabaseInfoService.Application.Languages;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Endpoints;

public static class LanguageEndpoints
{
    public static void MapLanguageEndpoints(this IEndpointRouteBuilder app)
    {
        app.MapGroup("languages");
        
        app.MapGet("/", GetAllLanguagesAsync)
            .Produces<IReadOnlyCollection<Language>>()
            .Produces(StatusCodes.Status400BadRequest)
            .WithName("getAllLanguages")
            .WithDisplayName("Get All Languages Supported by the Service")
            .WithDescription("For now Postgres and SQL Server are supported only");

        app.MapGet("/{id}", GetLanguageByIdAsync)
            .Produces<Language>()
            .Produces(StatusCodes.Status400BadRequest)
            .Produces(StatusCodes.Status404NotFound)
            .WithName("getLanguageById")
            .WithDisplayName("Get Language by Id");
    }
    
    private static async Task<IResult> GetAllLanguagesAsync(
        [FromServices] IGetAllLanguagesRequestHandler handler,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetAllLanguagesRequest();
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
    
    private static async Task<IResult> GetLanguageByIdAsync(
        [FromServices] IGetLanguageByIdRequestHandler handler,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var mediatorRequest = new GetLanguageByIdRequest(id);
        
        var result = await handler.HandleAsync(mediatorRequest, cancellationToken);
        
        return result.ToAspNetResult();
    }
}