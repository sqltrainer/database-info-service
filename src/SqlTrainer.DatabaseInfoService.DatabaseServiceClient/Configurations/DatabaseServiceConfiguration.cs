namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Configurations;

public sealed class DatabaseServiceConfiguration
{
    public required Uri Uri { get; init; }
}