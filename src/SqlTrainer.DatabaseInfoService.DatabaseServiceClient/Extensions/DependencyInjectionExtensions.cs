using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Configurations;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
    {
        var baseUrl = configuration.GetSection("Communication:DatabaseService").GetValue<string>("Url")
                      ?? throw new ArgumentNullException(nameof(configuration), "DatabaseService:Url is required.");
        
        return services
            .AddSingleton(new DatabaseServiceConfiguration { Uri = new Uri(baseUrl) })
            .AddTransient<IDatabasesService, Services.DatabasesService>()
            .AddTransient<IScriptService, Services.ScriptService>();
    }
}