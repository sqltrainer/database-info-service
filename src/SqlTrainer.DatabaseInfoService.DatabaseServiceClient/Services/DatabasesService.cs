using System.Net.Http.Json;
using DatabaseHelper.ScriptResults;
using Results;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Configurations;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Services;

public sealed class DatabasesService(IHttpClientFactory httpClientFactory, DatabaseServiceConfiguration configuration)
    : IDatabasesService
{
    private readonly Uri baseUri = configuration.Uri;

    public async Task<Result> CheckConnectionAsync(string language, string connectionString, CancellationToken cancellationToken = default)
    {
        using var httpClient = CreateHttpClient();
        
        var requestUrl = $"databases/check-connection?connectionString={connectionString}&language={language}";
        var response = await httpClient.GetAsync(requestUrl, cancellationToken);

        if (response.IsSuccessStatusCode)
            return Result.Success();

        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while checking the connection.");
    }

    public async Task<Result> CreateDatabaseAsync(string language, string connectionString, string databaseName, CancellationToken cancellationToken = default)
    {
        CreateDatabaseRequest request = new()
        {
            ConnectionString = connectionString,
            Name = databaseName,
            Language = language
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PostAsJsonAsync("databases", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while creating the database.");
    }

    public async Task<Result> DropDatabaseAsync(string language, string baseConnectionString, string databaseName, CancellationToken cancellationToken = default)
    {
        using var httpClient = CreateHttpClient();
        
        var requestUrl = $"databases/{databaseName}?connectionString={baseConnectionString}&language={language}";
        var response = await httpClient.DeleteAsync(requestUrl, cancellationToken);

        if (response.IsSuccessStatusCode)
            return Result.Success();

        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while dropping the database.");
    }

    public async Task<Result> RenameDatabaseAsync(string language, string connectionString, string oldName, string newName, CancellationToken cancellationToken = default)
    {
        RenameDatabaseRequest request = new()
        {
            ConnectionString = connectionString,
            OldName = oldName,
            NewName = newName,
            Language = language
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PutAsJsonAsync("databases/rename", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while renaming the database.");
    }

    public async Task<Result> CreateTableAsync(string language, string connectionString, Table table, CancellationToken cancellationToken = default)
    {
        CreateTableRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            Name = table.Name,
            Attributes = table.Attributes.Select(TableAttributeRequest.FromDomainAttribute)
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PostAsJsonAsync("tables", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while creating the table.");
    }

    public async Task<Result> AlterTableAsync(string language, string connectionString, Table oldTable, Table newTable, CancellationToken cancellationToken = default)
    {
        using var httpClient = CreateHttpClient();

        Result result;
        
        var isNameChanged = !oldTable.Name.Equals(newTable.Name);
        if (isNameChanged)
        {
            result = await RenameTableAsync(httpClient, language, connectionString, oldTable.Name, newTable.Name, cancellationToken);
            if (!result.IsSucceeded)
                return result;
        }

        result = await AlterTableAsync(httpClient, language, connectionString, oldTable, newTable, cancellationToken);
        
        if (result.IsFailed && isNameChanged)
        {
            // Renaming table back if field altering failed
            await RenameTableAsync(httpClient, language, connectionString, newTable.Name, oldTable.Name, cancellationToken);
        }

        return result;
    }

    private static async Task<Result> RenameTableAsync(HttpClient httpClient, string language, string connectionString, string oldName, string newName, CancellationToken cancellationToken)
    {
        var renameTableRequest = new RenameTableRequest
        {
            ConnectionString = connectionString,
            Language = language,
            OldName = oldName,
            NewName = newName
        };
        var renameResponse = await httpClient.PutAsJsonAsync("tables/rename", renameTableRequest, cancellationToken);

        if (renameResponse.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await renameResponse.Content.ReadFromJsonAsync<Result>(cancellationToken);
        return result ?? Result.Failed("An error occurred while renaming the table.");
    }

    private static async Task<Result> AlterTableAsync(HttpClient httpClient, string language, string connectionString, Table oldTable, Table newTable, CancellationToken cancellationToken)
    {
        var oldTableAttributeIds = oldTable.Attributes.Select(a => a.Id).ToList();
        var attributeVersions = oldTable.Attributes
            .Select(oa => new AlterTableAttributeVersions(oa, newTable.Attributes.FirstOrDefault(na => na.Id == oa.Id)))
            .ToList();
        
        var newAttributeVersions = newTable.Attributes
            .Where(na => !oldTableAttributeIds.Contains(na.Id))
            .Select(na => new AlterTableAttributeVersions(na));
        
        attributeVersions.AddRange(newAttributeVersions);
        
        AlterTableRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            Name = newTable.Name,
            Attributes = attributeVersions
            
        };
        var response = await httpClient.PutAsJsonAsync("tables", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        return result ?? Result.Failed("An error occurred while altering the table.");
    }

    public async Task<Result> DropTableAsync(string language, string connectionString, string tableName, CancellationToken cancellationToken = default)
    {
        using var httpClient = CreateHttpClient();
        
        var requestUrl = $"tables/{tableName}?connectionString={connectionString}&language={language}";
        var response = await httpClient.DeleteAsync(requestUrl, cancellationToken);

        if (response.IsSuccessStatusCode)
            return Result.Success();

        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        
        return result ?? Result.Failed("An error occurred while dropping the table.");
    }

    public async Task<Result> InsertDataAsync(string language, string connectionString, Table table, string jsonData, CancellationToken cancellationToken = default)
    {
        InsertDataToTableRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            TableName = table.Name,
            Attributes = table.Attributes.Select(TableAttributeRequest.FromDomainAttribute),
            JsonData = jsonData
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PostAsJsonAsync("data/insert", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        return result ?? Result.Failed("An error occurred while dropping the table.");
    }

    public async Task<Result> UpdateDataAsync(string language, string connectionString, Table table, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default)
    {
        UpdateDataIntoTableRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            TableName = table.Name,
            Attributes = table.Attributes.Select(TableAttributeRequest.FromDomainAttribute),
            OldJsonData = oldJsonData,
            NewJsonData = newJsonData
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PutAsJsonAsync("data/update", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        return result ?? Result.Failed("An error occurred while updating the data.");
    }

    public async Task<Result> DeleteDataAsync(string language, string connectionString, Table table, string jsonData, CancellationToken cancellationToken = default)
    {
        DeleteDataFromTableRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            TableName = table.Name,
            Attributes = table.Attributes.Select(TableAttributeRequest.FromDomainAttribute),
            JsonData = jsonData
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PostAsJsonAsync("data/delete", request, cancellationToken);
        
        if (response.IsSuccessStatusCode)
            return Result.Success();
        
        var result = await response.Content.ReadFromJsonAsync<Result>(cancellationToken);
        return result ?? Result.Failed("An error occurred while deleting the data.");
    }

    public async Task<Result<ScriptResult>> SelectDataAsync(string language, string connectionString, Table table, CancellationToken cancellationToken = default)
    {
        DataRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            TableName = table.Name,
            Attributes = table.Attributes.Select(TableAttributeRequest.FromDomainAttribute)
        };
        
        using var httpClient = CreateHttpClient();
        
        var response = await httpClient.PostAsJsonAsync("data/select", request, cancellationToken);

        return await response.Content.ReadFromJsonAsync<Result<ScriptResult>>(cancellationToken) 
               ?? Result.Failed<ScriptResult>("An error occurred while selecting the data.");
    }

    private HttpClient CreateHttpClient()
    {
        var client = httpClientFactory.CreateClient();
        client.BaseAddress = baseUri;
        return client;
    }
}