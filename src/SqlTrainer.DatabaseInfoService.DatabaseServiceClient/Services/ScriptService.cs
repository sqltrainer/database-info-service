using System.Net.Http.Json;
using DatabaseHelper.ScriptResults;
using Results;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Configurations;
using SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Services;

public sealed class ScriptService(IHttpClientFactory httpClientFactory, DatabaseServiceConfiguration configuration) 
    : IScriptService
{
    private readonly Uri baseUri = configuration.Uri;
    
    public async Task<Result<ScriptResult>> ExecuteAsync(string language, string connectionString, string script, CancellationToken cancellationToken = default)
    {
        ExecuteRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            Script = script
        };
        
        using var client = CreateHttpClient();
        
        var response = await client.PostAsJsonAsync("scripts", request, cancellationToken);
        
        return await response.Content.ReadFromJsonAsync<Result<ScriptResult>>(cancellationToken) 
               ?? Result.Failed<ScriptResult>("An error occurred while executing the script.");
    }

    public async Task<Result<IReadOnlyCollection<ScriptResult>>> ExecuteAsync(string language, string connectionString, IEnumerable<string> scripts, CancellationToken cancellationToken = default)
    {
        ExecuteBatchRequest request = new()
        {
            ConnectionString = connectionString,
            Language = language,
            Scripts = scripts
        };
        
        using var client = CreateHttpClient();
        
        var response = await client.PostAsJsonAsync("scripts/batch", request, cancellationToken);
        
        return await response.Content.ReadFromJsonAsync<Result<IReadOnlyCollection<ScriptResult>>>(cancellationToken) 
               ?? Result.Failed<IReadOnlyCollection<ScriptResult>>("An error occurred while executing the scripts.");
    }

    private HttpClient CreateHttpClient()
    {
        var client = httpClientFactory.CreateClient();
        client.BaseAddress = baseUri;
        return client;
    }
}