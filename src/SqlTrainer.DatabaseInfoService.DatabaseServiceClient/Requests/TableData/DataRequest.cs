using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public class DataRequest : DatabaseServiceRequest
{
    [JsonPropertyName("table")]
    public required string TableName { get; init; }
    
    [JsonPropertyName("attributes")]
    public required IEnumerable<TableAttributeRequest> Attributes { get; init; }
}