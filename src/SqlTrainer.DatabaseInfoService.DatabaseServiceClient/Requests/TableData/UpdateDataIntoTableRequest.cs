using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public class UpdateDataIntoTableRequest : DataRequest
{
    [JsonPropertyName("oldData")]
    public required string OldJsonData { get; init; }
    
    [JsonPropertyName("newData")]
    public required string NewJsonData { get; init; }
}