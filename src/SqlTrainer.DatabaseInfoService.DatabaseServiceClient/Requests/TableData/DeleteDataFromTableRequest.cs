using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class DeleteDataFromTableRequest : DataRequest
{
    [JsonPropertyName("data")]
    public required string JsonData { get; init; }
}