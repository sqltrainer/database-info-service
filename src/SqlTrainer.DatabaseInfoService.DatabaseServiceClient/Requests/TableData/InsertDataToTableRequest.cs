using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class InsertDataToTableRequest : DataRequest
{
    [JsonPropertyName("data")]
    public required string JsonData { get; init; }
}