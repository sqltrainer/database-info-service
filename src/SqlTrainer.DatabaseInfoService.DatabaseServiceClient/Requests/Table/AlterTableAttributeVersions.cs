using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed record AlterTableAttributeVersions(
    [property: JsonPropertyName("old")] TableAttributeRequest? Old,
    [property: JsonPropertyName("new")] TableAttributeRequest? New)
{
    public AlterTableAttributeVersions(Domain.Models.Attribute? oldAttribute, Domain.Models.Attribute? newAttribute)
        : this(oldAttribute is null ? null : TableAttributeRequest.FromDomainAttribute(oldAttribute), 
            newAttribute is null ? null : TableAttributeRequest.FromDomainAttribute(newAttribute))
    {
    }

    public AlterTableAttributeVersions(Domain.Models.Attribute newAttribute)
        : this(null, TableAttributeRequest.FromDomainAttribute(newAttribute))
    {
    }
}