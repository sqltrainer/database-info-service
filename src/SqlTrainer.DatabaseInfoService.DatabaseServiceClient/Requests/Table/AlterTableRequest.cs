using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class AlterTableRequest : DatabaseServiceRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("attributes")]
    public required IEnumerable<AlterTableAttributeVersions> Attributes { get; init; }
}