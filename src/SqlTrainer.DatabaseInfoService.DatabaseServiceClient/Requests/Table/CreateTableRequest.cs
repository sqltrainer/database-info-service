using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class CreateTableRequest : DatabaseServiceRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("attributes")]
    public required IEnumerable<TableAttributeRequest> Attributes { get; init; }
}