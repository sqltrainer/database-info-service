using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class RenameTableRequest : DatabaseServiceRequest
{
    [JsonPropertyName("oldName")]
    public required string OldName { get; init; }
    
    [JsonPropertyName("newName")]
    public required string NewName { get; init; }
}