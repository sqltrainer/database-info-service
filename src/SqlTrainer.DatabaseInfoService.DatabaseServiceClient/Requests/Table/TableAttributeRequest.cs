using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public class TableAttributeRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
    
    [JsonPropertyName("type")]
    public required string Type { get; init; }
    
    [JsonPropertyName("default")]
    public required string? DefaultValue { get; init; }
    
    [JsonPropertyName("length")]
    public required int? VarcharNumberOfSymbols { get; init; }
    
    [JsonPropertyName("isPrimaryKey")]
    public required bool IsPrimaryKey { get; init; }
    
    [JsonPropertyName("notNull")]
    public required bool IsNotNull { get; init; }
    
    [JsonPropertyName("unique")]
    public required bool IsUnique { get; init; }
    
    [JsonPropertyName("foreignKey")]
    public required string? ForeignKeyName { get; init; }
    
    [JsonPropertyName("reference")]
    public required string? ReferenceTableName { get; init; }
    
    [JsonPropertyName("order")]
    public required int Order { get; init; }

    public static TableAttributeRequest FromDomainAttribute(Domain.Models.Attribute attribute) => new()
    {
        Name = attribute.Name,
        Type = attribute.Type,
        DefaultValue = attribute.DefaultValue,
        VarcharNumberOfSymbols = attribute.VarcharNumberOfSymbols,
        IsPrimaryKey = attribute.IsPrimaryKey,
        IsNotNull = attribute.IsNotNull,
        IsUnique = attribute.IsUnique,
        ForeignKeyName = attribute.ForeignKey?.Name,
        ReferenceTableName = attribute.Table?.Name,
        Order = attribute.Order
    };
}