using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public abstract class DatabaseServiceRequest
{
    [JsonPropertyName("connectionString")]
    public required string ConnectionString { get; init; }
    
    [JsonPropertyName("language")]
    public required string Language { get; init; }
}