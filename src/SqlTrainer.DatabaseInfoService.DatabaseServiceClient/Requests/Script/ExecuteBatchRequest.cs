using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class ExecuteBatchRequest : DatabaseServiceRequest
{
    [JsonPropertyName("scripts")]
    public required IEnumerable<string> Scripts { get; init; }
}