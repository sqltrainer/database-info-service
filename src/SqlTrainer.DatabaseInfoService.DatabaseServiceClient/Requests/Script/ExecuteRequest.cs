using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class ExecuteRequest : DatabaseServiceRequest
{
    [JsonPropertyName("script")]
    public required string Script { get; init; }
}