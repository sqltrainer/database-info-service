using System.Text.Json.Serialization;

namespace SqlTrainer.DatabaseInfoService.DatabaseServiceClient.Requests;

public sealed class CreateDatabaseRequest : DatabaseServiceRequest
{
    [JsonPropertyName("name")]
    public required string Name { get; init; }
}