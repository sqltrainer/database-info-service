using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

using TableAttribute = SqlTrainer.DatabaseInfoService.Domain.Models.Attribute;

namespace SqlTrainer.DatabaseInfoService.Persistence.Data;

public class DatabaseInfoDbContext(DbContextOptions<DatabaseInfoDbContext> options) : DbContext(options), IDatabaseInfoDbContext
{
    public DbSet<Language> Languages { get; set; }
    public DbSet<Database> Databases { get; set; }
    public DbSet<Table> Tables { get; set; }
    public DbSet<TableAttribute> Attributes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ConfigureLanguages(modelBuilder.Entity<Language>());
        ConfigureDatabases(modelBuilder.Entity<Database>());
        ConfigureTables(modelBuilder.Entity<Table>());
        ConfigureAttributes(modelBuilder.Entity<TableAttribute>());
    }
    
    private static void ConfigureLanguages(EntityTypeBuilder<Language> entity)
    {
        entity.ToTable("languages");
        entity.HasKey(e => e.Id);
        
        entity.Property(e => e.Id).HasColumnName("id").ValueGeneratedOnAdd();
        entity.Property(e => e.Name).HasColumnName("name").IsRequired().HasMaxLength(50);
        entity.Property(e => e.ShortName).HasColumnName("short_name").IsRequired().HasMaxLength(50);
        entity.Property(e => e.BaseConnectionString).HasColumnName("base_connection_string").IsRequired().HasMaxLength(500);
    }
    
    private static void ConfigureDatabases(EntityTypeBuilder<Database> entity)
    {
        entity.ToTable("databases");
        entity.HasKey(e => e.Id);
        
        entity.Property(e => e.Id).HasColumnName("id").ValueGeneratedOnAdd();
        entity.Property(e => e.Name).HasColumnName("name").IsRequired().HasMaxLength(50);
        entity.Property(e => e.LanguageId).HasColumnName("language_id").IsRequired();
        entity.Property(e => e.ConnectionString).HasColumnName("connection_string").IsRequired().HasMaxLength(500);
        
        entity.HasOne(e => e.Language).WithMany().HasForeignKey(e => e.LanguageId);
    }
    
    private static void ConfigureTables(EntityTypeBuilder<Table> entity)
    {
        entity.ToTable("tables");
        entity.HasKey(e => e.Id);
        
        entity.Property(e => e.Id).HasColumnName("id").ValueGeneratedOnAdd();
        entity.Property(e => e.Name).HasColumnName("name").IsRequired().HasMaxLength(50);
        entity.Property(e => e.DatabaseId).HasColumnName("database_id").IsRequired();
        
        entity.HasOne(e => e.Database).WithMany().HasForeignKey(e => e.DatabaseId);
    }
    
    private static void ConfigureAttributes(EntityTypeBuilder<TableAttribute> entity)
    {
        entity.ToTable("attributes");
        entity.HasKey(e => e.Id);
        
        entity.Property(e => e.Id).HasColumnName("id").ValueGeneratedOnAdd();
        entity.Property(e => e.Name).HasColumnName("name").IsRequired().HasMaxLength(50);
        entity.Property(e => e.Type).HasColumnName("type").IsRequired().HasMaxLength(50);
        entity.Property(e => e.VarcharNumberOfSymbols).HasColumnName("number_of_symbols");
        entity.Property(e => e.IsNotNull).HasColumnName("is_not_null").IsRequired();
        entity.Property(e => e.IsUnique).HasColumnName("is_unique").IsRequired();
        entity.Property(e => e.DefaultValue).HasColumnName("default_value").HasMaxLength(200);
        entity.Property(e => e.IsPrimaryKey).HasColumnName("is_primary_key").IsRequired();
        entity.Property(e => e.Order).HasColumnName("order");
        entity.Property(e => e.TableId).HasColumnName("table_id").IsRequired();
        entity.Property(e => e.ForeignKeyId).HasColumnName("foreign_key_id");
        
        entity.HasOne(e => e.Table).WithMany().HasForeignKey(e => e.TableId);
        
        entity.HasOne(e => e.ForeignKeyAttribute).WithMany().HasForeignKey(e => e.ForeignKeyId);
        entity.Ignore(e => e.ForeignKey);
    }
}