using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Persistence.Data;

namespace SqlTrainer.DatabaseInfoService.Persistence.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<DatabaseInfoDbContext>(o => o.UseDatabaseInfoDb(configuration));
        
        return services.AddScoped<IDatabaseInfoDbContext, DatabaseInfoDbContext>();
    }
    
    public static void UseDatabaseInfoDb(this DbContextOptionsBuilder options, IConfiguration configuration)
    {
        options.UseNpgsql(configuration.GetConnectionString("DatabaseInfoDb"));
    }
}