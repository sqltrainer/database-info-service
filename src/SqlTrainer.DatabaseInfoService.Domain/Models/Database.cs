﻿using Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Domain.Models;

public sealed class Database(Guid id) : IdModel(id), IEquatable<Database>
{
    public Database() : this(default)
    {
    }

    private readonly List<Table> tables = [];
    
    public required string Name { get; set; }
    public required  string ConnectionString { get; init; }
    public required Guid LanguageId { get; init; }
    public Language? Language { get; private set; }
    public IReadOnlyCollection<Table> Tables => tables.AsReadOnly();

    public Database WithLanguage(Language language)
    {
        Language = language;
        return this;
    }

    public override bool Equals(object? obj) => obj is Database database && Equals(database);
    public bool Equals(Database? other) => other is not null && (other.Id == Id || other.Name.Equals(Name) && other.LanguageId == LanguageId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Database? left, Database? right) => left is not null && left.Equals(right);
    public static bool operator !=(Database? left, Database? right) => !(left == right);
    
    public override string ToString() => Name;
}