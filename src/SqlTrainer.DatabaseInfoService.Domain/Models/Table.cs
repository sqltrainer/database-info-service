﻿using Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Domain.Models;

public sealed class Table(Guid id) : IdModel(id), IEquatable<Table>
{
    public Table() : this(default)
    {
    }
    
    private readonly List<Attribute> attributes = [];
    
    public required string Name { get; set; }
    public required Guid DatabaseId { get; init; }
    public Database? Database { get; private set; }
    public IReadOnlyCollection<Attribute> Attributes => attributes.AsReadOnly();

    public Table WithDatabase(Database database)
    {
        Database = database;
        return this;
    }
    
    public Table WithAttributes(IEnumerable<Attribute> attrs)
    {
        attributes.Clear();
        attributes.AddRange(attrs);
        return this;
    }
    
    public void AddAttribute(Attribute attribute) => attributes.Add(attribute);
    
    public void AddAttributes(IEnumerable<Attribute> attrs) => attributes.AddRange(attrs);
    
    public override bool Equals(object? obj) => obj is Table table && Equals(table);
    public bool Equals(Table? other) => other is not null && (other.Id == Id || other.Name.Equals(Name) && other.DatabaseId == DatabaseId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Table? left, Table? right) => left is not null && left.Equals(right);
    public static bool operator !=(Table? left, Table? right) => !(left == right);
    
    public override string ToString() => Name;
}