﻿using Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Domain.Models;

public sealed class Attribute(Guid id) : IdModel(id), IEquatable<Attribute>
{
    public Attribute() : this(default)
    {
    }
    
    private ForeignKey? foreignKey;
    
    public required string Name { get; init; }
    public required bool IsPrimaryKey { get; init; }
    public required bool IsNotNull { get; init; }
    public required bool IsUnique { get; init; }
    public string? DefaultValue { get; init; }
    public required string Type { get; init; }
    public int? VarcharNumberOfSymbols { get; init; }
    public Guid? ForeignKeyId { get; init; }
    public required Guid TableId { get; init; }
    public required int Order { get; init; }
    public Table? Table { get; private set; }
    public Attribute? ForeignKeyAttribute { get; private set; }
    public ForeignKey? ForeignKey => ForeignKeyAttribute is not null 
        ? foreignKey ??= CreateForeignKey(ForeignKeyAttribute) 
        : null;

    public Attribute WithTable(Table table)
    {
        Table = table;
        return this;
    }
    
    public Attribute WithForeignKey(Attribute attribute)
    {
        ForeignKeyAttribute = attribute;
        return this;
    }
    
    private static ForeignKey CreateForeignKey(Attribute attribute)
    {
        var key = new ForeignKey(attribute.Id)
        {
            Name = attribute.Name,
            TableId = attribute.TableId
        };
        
        if (attribute.Table is not null)
            key.WithTable(attribute.Table);
        
        return key;
    }

    public override bool Equals(object? obj) => obj is Attribute attribute && Equals(attribute);
    public bool Equals(Attribute? other) => other is not null && (other.Id == Id || other.Name.Equals(Name) && other.TableId == TableId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Attribute? left, Attribute? right) => left is not null && left.Equals(right);
    public static bool operator !=(Attribute? left, Attribute? right) => !(left == right);
    
    public override string ToString() => Name;
}