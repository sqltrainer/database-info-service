﻿using Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Domain.Models;

public sealed class ForeignKey(Guid id) : IdModel(id), IEquatable<ForeignKey>
{
    public ForeignKey() : this(default)
    {
    }
    
    public required string Name { get; init; }
    public required Guid TableId { get; init; }
    public Table? Table { get; private set; }
    
    public ForeignKey WithTable(Table table)
    {
        Table = table;
        return this;
    }

    public override bool Equals(object? obj) => obj is ForeignKey foreignKey && Equals(foreignKey);
    public bool Equals(ForeignKey? other) => other is not null && (other.Id == Id || other.Name.Equals(Name) && other.TableId == TableId);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(ForeignKey? left, ForeignKey? right) => left is not null && left.Equals(right);
    public static bool operator !=(ForeignKey? left, ForeignKey? right) => !(left == right);
    
    public override string ToString() => Name;
}