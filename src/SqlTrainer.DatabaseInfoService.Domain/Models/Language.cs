﻿using Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Domain.Models;

public sealed class Language(Guid id) : IdModel(id), IEquatable<Language>
{
    public Language() : this(default)
    {
    }
    
    public required string Name { get; init; }
    public required string ShortName { get; init; }
    public required string BaseConnectionString { get; init; }

    public override bool Equals(object? obj) => obj is Language language && Equals(language);
    public bool Equals(Language? other) => other is not null && (other.Id == Id || other.Name.Equals(Name));
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Language? left, Language? right) => left is not null && left.Equals(right);
    public static bool operator !=(Language? left, Language? right) => !(left == right);

    public override string ToString() => Name;
}