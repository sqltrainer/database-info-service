using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;

namespace SqlTrainer.DatabaseInfoService.Application.Attributes;

using TableAttribute = Domain.Models.Attribute;

public sealed record GetPrimaryKeysByDatabaseRequest(Guid DatabaseId) : IRequest;

public interface IGetPrimaryKeysByDatabaseRequestHandler : IRequestHandler<GetPrimaryKeysByDatabaseRequest, IReadOnlyCollection<TableAttribute>>;

public sealed class GetPrimaryKeysByDatabaseRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetPrimaryKeysByDatabaseRequestHandler> logger)
    : IGetPrimaryKeysByDatabaseRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<TableAttribute>>> HandleAsync(GetPrimaryKeysByDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var attributes = await dbContext.Attributes.AsNoTracking()
                .Include(a => a.Table)
                .Where(a => a.Table!.DatabaseId == request.DatabaseId && a.IsPrimaryKey)
                .ToListAsync(cancellationToken);
            
            return Result.Success<IReadOnlyCollection<TableAttribute>>(attributes);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Primary keys has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<TableAttribute>>(ex, "Cannot retrieve primary keys.");
        }
    }
}