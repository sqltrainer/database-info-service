using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;

namespace SqlTrainer.DatabaseInfoService.Application.Attributes;

using TableAttribute = Domain.Models.Attribute;

public sealed record GetAttributesByTableRequest(Guid TableId) : IRequest;

public interface IGetAttributesByTableRequestHandler : IRequestHandler<GetAttributesByTableRequest, IReadOnlyCollection<Domain.Models.Attribute>>;

public sealed class GetAttributesByTableRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetAttributesByTableRequestHandler> logger)
    : IGetAttributesByTableRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<TableAttribute>>> HandleAsync(GetAttributesByTableRequest request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var attributes = await dbContext.Attributes.AsNoTracking().Where(a => a.TableId == request.TableId).ToListAsync(cancellationToken);
            return Result.Success<IReadOnlyCollection<TableAttribute>>(attributes);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Attributes has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<TableAttribute>>(ex, "Cannot retrieve attributes.");
        }
    }
}