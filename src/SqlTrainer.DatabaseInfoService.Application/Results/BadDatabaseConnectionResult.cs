using Results;

namespace SqlTrainer.DatabaseInfoService.Application.Results;

public class BadDatabaseConnectionResult(string languageName, string databaseName) : Result([
    new Error($"Could not connect to {databaseName} database for {languageName} language.")
])
{
    public static BadDatabaseConnectionResult Create(string languageName, string databaseName) => new(languageName, databaseName);

    public static BadDatabaseConnectionResult<T> Create<T>(string languageName, string databaseName) => new(languageName, databaseName);
}

public class BadDatabaseConnectionResult<T>(string languageName, string databaseName) : Result<T>([
    new Error($"Could not connect to {databaseName} database for {languageName} language.")
]);