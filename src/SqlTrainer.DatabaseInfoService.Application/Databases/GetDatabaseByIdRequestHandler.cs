using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Databases;

public sealed record GetDatabaseByIdRequest(Guid Id): IRequest;

public interface IGetDatabaseByIdRequestHandler : IRequestHandler<GetDatabaseByIdRequest, Database>;

public sealed class GetDatabaseByIdRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetDatabaseByIdRequestHandler> logger)
    : IGetDatabaseByIdRequestHandler
{
    public async Task<IFinalResult<Database>> HandleAsync(GetDatabaseByIdRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var database = await dbContext.Databases.AsNoTracking().FirstOrDefaultAsync(d => d.Id == request.Id, cancellationToken);
            
            return database is not null ? Result.Success(database) : Result.NotFound<Database>();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Database has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<Database>(ex, "Cannot retrieve database.");
        }
    }
}