using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Configurations;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Databases;

public sealed record DeleteDatabaseRequest(Guid Id) : IRequest;

public interface IDeleteDatabaseRequestHandler : IRequestHandler<DeleteDatabaseRequest>;

public sealed class DeleteDatabaseRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    LanguagesConfigurations configuration,
    ILogger<DeleteDatabaseRequestHandler> logger)
    : IDeleteDatabaseRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var database = await dbContext.Databases.AsNoTracking()
                .Include(d => d.Language)
                .FirstOrDefaultAsync(d => d.Id == request.Id, cancellationToken);
            
            if (database is null)
                return Result.Failed("Database does not exist");
            
            dbContext.Databases.Remove(database);
            
            var defaultDatabaseName = configuration[database.Language!]?.Database;
            if (string.IsNullOrWhiteSpace(defaultDatabaseName))
                return Result.Failed("Could not get default database name");
            
            var connectionString = database.ConnectionString.Replace(database.Name, defaultDatabaseName);
            var dropDatabaseResult = await databasesService.DropDatabaseAsync(database.Language!.ShortName, connectionString, database.Name, cancellationToken);
            if (!dropDatabaseResult.IsSucceeded)
                return Result.Failed("Database could not be deleted");
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Database has not been deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Cannot delete database.");
        }
    }
}