using Results;
using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Databases;

public sealed record GetAllDatabasesRequest(
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllDatabasesRequestHandler : IRequestHandler<GetAllDatabasesRequest, IReadOnlyCollection<Database>>;

public sealed class GetAllDatabasesRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetAllDatabasesRequestHandler> logger)
    : IGetAllDatabasesRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Database>>> HandleAsync(GetAllDatabasesRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var databases = await dbContext.Databases.AsNoTracking()
                .Where(d => d.Name.Contains(request.SearchTerm, StringComparison.OrdinalIgnoreCase))
                .SortAndPaginate(request)
                .ToListAsync(cancellationToken);
            
            return Result.Success(databases);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Databases have not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Database>>(ex, "Cannot retrieve databases.");
        }
    }
}