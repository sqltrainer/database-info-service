using Results;
using Results.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Configurations;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Results;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Databases;

public sealed record AddDatabaseRequest(string Name, Guid LanguageId) : IRequest;

public interface IAddDatabaseRequestHandler : IRequestHandler<AddDatabaseRequest, Guid>;

public sealed class AddDatabaseRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    LanguagesConfigurations configuration,
    ILogger<AddDatabaseRequestHandler> logger)
    : IAddDatabaseRequestHandler
{
    private string? baseConnectionString;
    private bool isBaseConnectionGood;
    
    public async Task<IFinalResult<Guid>> HandleAsync(AddDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (validationResult.IsFailed)
                return validationResult.OfType<Guid>();
            
            var language = validationResult.Value;
            
            baseConnectionString = configuration.FormatConnectionString(language)!;
            var checkConnectionResult = await databasesService.CheckConnectionAsync(language.ShortName, baseConnectionString, cancellationToken);
            isBaseConnectionGood = checkConnectionResult.IsSucceeded;
            if (!isBaseConnectionGood)
                return BadDatabaseConnectionResult.Create<Guid>(language.ShortName, request.Name);

            await databasesService.CreateDatabaseAsync(language.ShortName, baseConnectionString, request.Name, cancellationToken);
            var connectionString = configuration.FormatConnectionString(language, request.Name)!;
            checkConnectionResult = await databasesService.CheckConnectionAsync(language.ShortName, connectionString, cancellationToken);
            var isConnectionGood = checkConnectionResult.IsSucceeded;
            if (!isConnectionGood)
                return BadDatabaseConnectionResult.Create<Guid>(language.ShortName, request.Name);

            var database = new Database
            {
                Name = request.Name,
                ConnectionString = connectionString,
                LanguageId = request.LanguageId
            };
        
            await dbContext.Databases.AddAsync(database, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult.OfType(database.Id);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Database has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "Cannot add database.");
        }
    }

    private async Task<ValidationResult<Language>> ValidateAsync(AddDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        var validationResult = new ValidationResult<Language>();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required");

        if (request.LanguageId == default)
        {
            validationResult.Fail(nameof(request.LanguageId), "LanguageId is required");
            return validationResult;
        }
        
        var language = await dbContext.Languages.AsNoTracking().FirstOrDefaultAsync(l => l.Id == request.LanguageId, cancellationToken);
        if (language is null)
            validationResult.Fail(nameof(request.LanguageId), "Language does not exist");
        else
            validationResult.Complete(language);
        
        return validationResult;
    }
}