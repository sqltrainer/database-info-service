using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Configurations;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Databases;

public sealed record UpdateDatabaseRequest(Guid Id, string Name) : IRequest;

public interface IUpdateDatabaseRequestHandler : IRequestHandler<UpdateDatabaseRequest>;

public sealed class UpdateDatabaseRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    LanguagesConfigurations configuration,
    ILogger<UpdateDatabaseRequestHandler> logger)
    : IUpdateDatabaseRequestHandler
{
    
    public async Task<IFinalResult> HandleAsync(UpdateDatabaseRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult;
            
            var database = validationResult.Value;
        
            database.Name = request.Name;
            
            var defaultDatabaseName = configuration[database.Language!]?.Database;
            if (string.IsNullOrWhiteSpace(defaultDatabaseName))
                return Result.Failed("Could not get default database name");

            var connectionString = database.ConnectionString.Replace(database.Name, defaultDatabaseName);
            var renameResult = await databasesService.RenameDatabaseAsync(database.Language!.ShortName, connectionString, database.Name, request.Name, cancellationToken);
            if (!renameResult.IsSucceeded)
                return Result.Failed($"Database could not be renamed to {request.Name}");
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Database has not been updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Cannot update database.");
        }
    }
    
    private async Task<IFinalResult<Database>> ValidateAsync(UpdateDatabaseRequest request, CancellationToken cancellationToken)
    {
        var validationResult = new ValidationResult<Database>();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required");

        if (request.Id == default)
        {
            validationResult.Fail(nameof(request.Id), "Id is required");
            return validationResult;
        }

        var database = await dbContext.Databases.Include(d => d.Language).FirstOrDefaultAsync(d => d.Id == request.Id, cancellationToken);
        if (database is null)
            validationResult.Fail(nameof(request.Id), "Database does not exist");
        else
            validationResult.Complete(database);
        
        return validationResult;
    }
}