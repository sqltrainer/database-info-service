using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Configurations;

public class LanguagesConfigurations
{
    public required Dictionary<string, LanguageSettings> Languages { get; init; }
    
    public LanguageSettings? this[Language language] => this[language.Name] ?? this[language.ShortName];
    public LanguageSettings? this[string languageName] => Languages.GetValueOrDefault(languageName);
    
    public string? FormatConnectionString(Language language, string? databaseName = null) =>
        FormatConnectionString(language.BaseConnectionString, language.Name, databaseName) 
        ?? FormatConnectionString(language.BaseConnectionString, language.ShortName, databaseName);

    private string? FormatConnectionString(string connectionString, string languageName, string? databaseName = null)
    {
        return Languages.TryGetValue(languageName, out var languageSettings)
            ? languageSettings.FormatConnectionString(connectionString, databaseName)
            : null;
    }
}

public sealed class LanguageSettings
{
    public required string Server { get; init; }
    public required int Port { get; init; }
    public required string Database { get; init; }
    public required string User { get; init; }
    public required string Password { get; init; }
    
    public string FormatConnectionString(string connectionString, string? databaseName = null) =>
        connectionString
            .Replace("{Server}", Server)
            .Replace("{Port}", Port.ToString())
            .Replace("{Database}", databaseName ?? Database)
            .Replace("{User}", User)
            .Replace("{Password}", Password);
}