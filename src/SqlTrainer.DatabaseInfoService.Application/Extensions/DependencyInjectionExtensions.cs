using System.Reflection;
using Application.Mediator.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.DatabaseInfoService.Application.Configurations;
using SqlTrainer.DatabaseInfoService.Application.Helpers;
using SqlTrainer.DatabaseInfoService.Application.Scripts;
using SqlTrainer.DatabaseInfoService.Application.Tables.Data;

namespace SqlTrainer.DatabaseInfoService.Application.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
    {
        Dictionary<string, LanguageSettings> languageSettings = [];
        foreach (var section in configuration.GetSection("Languages").GetChildren())
        {
            var name = section.Key;
            var server = section.GetValue<string>("Server")
                         ?? throw new ArgumentNullException(nameof(configuration), $"Server for {name} is required.");
            var port = section.GetValue<int>("Port");
            var database = section.GetValue<string>("Database")
                           ?? throw new ArgumentNullException(nameof(configuration), $"Database for {name} is required.");
            var user = section.GetValue<string>("User")
                       ?? throw new ArgumentNullException(nameof(configuration), $"User for {name} is required.");
            var password = section.GetValue<string>("Password")
                           ?? throw new ArgumentNullException(nameof(configuration), $"Password for {name} is required.");

            languageSettings.Add(name, new LanguageSettings
            {
                Server = server,
                Port = port,
                Database = database,
                User = user,
                Password = password
            });
        }

        services
            .AddSingleton(new LanguagesConfigurations { Languages = languageSettings })
            .AddTransient<IForeignKeyHelper, ForeignKeyHelper>()
            .AddTransient<ITableDataRequestValidator, TableDataRequestValidator>()
            .AddTransient<IScriptRequestValidator, ScriptRequestValidator>();
        
        services.InjectRequestStuff(Assembly.GetExecutingAssembly());

        return services;
    }
}