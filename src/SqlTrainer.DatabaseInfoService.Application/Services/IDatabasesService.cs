using DatabaseHelper.ScriptResults;
using Results;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Services;

public interface IDatabasesService
{
    Task<Result> CheckConnectionAsync(string language, string connectionString, CancellationToken cancellationToken = default);
    Task<Result> CreateDatabaseAsync(string language, string connectionString, string databaseName, CancellationToken cancellationToken = default);
    Task<Result> DropDatabaseAsync(string language, string baseConnectionString, string databaseName, CancellationToken cancellationToken = default);
    Task<Result> RenameDatabaseAsync(string language, string connectionString, string oldName, string newName, CancellationToken cancellationToken = default);
    Task<Result> CreateTableAsync(string language, string connectionString, Table table, CancellationToken cancellationToken = default);
    Task<Result> AlterTableAsync(string language, string connectionString, Table oldTable, Table newTable, CancellationToken cancellationToken = default);
    Task<Result> DropTableAsync(string language, string connectionString, string tableName, CancellationToken cancellationToken = default);
    Task<Result> InsertDataAsync(string language, string connectionString, Table table, string jsonData, CancellationToken cancellationToken = default);
    Task<Result> UpdateDataAsync(string language, string connectionString, Table table, string oldJsonData, string newJsonData, CancellationToken cancellationToken = default);
    Task<Result> DeleteDataAsync(string language, string connectionString, Table table, string jsonData, CancellationToken cancellationToken = default);
    Task<Result<ScriptResult>> SelectDataAsync(string language, string connectionString, Table table, CancellationToken cancellationToken = default);
}