using DatabaseHelper.ScriptResults;
using Results;

namespace SqlTrainer.DatabaseInfoService.Application.Services;

public interface IScriptService
{
    Task<Result<ScriptResult>> ExecuteAsync(string language, string connectionString, string script, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<ScriptResult>>> ExecuteAsync(string language, string connectionString, IEnumerable<string> scripts, CancellationToken cancellationToken = default);
}