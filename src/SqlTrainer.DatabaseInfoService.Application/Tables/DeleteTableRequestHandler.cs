using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

public sealed record DeleteTableRequest(Guid Id) : IRequest;

public interface IDeleteTableRequestHandler : IRequestHandler<DeleteTableRequest>;

public sealed class DeleteTableRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    ILogger<DeleteTableRequestHandler> logger)
    : IDeleteTableRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteTableRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var table = await dbContext.Tables.FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (table is null)
                return Result.Failed("Table does not exist");
        
            var database = await dbContext.Databases.AsNoTracking().Include(d => d.Language).FirstOrDefaultAsync(d => d.Id == table.DatabaseId, cancellationToken);
            if (database is null)
                return Result.Failed("Database does not exist");
        
            var result = await databasesService.DropTableAsync(database.Language!.ShortName, database.ConnectionString, table.Name, cancellationToken);
            if (!result.IsSucceeded)
            {
                logger.LogError("Table has not been deleted. Request: {Request}. Problems: {Problems}", request, string.Join("; ", result.Problems));
                return Result.Failed("Cannot drop table.");
            }
            
            dbContext.Tables.Remove(table);
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Success();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Table has not been deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Cannot delete table.");
        }
    }
}