using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

public sealed record GetTableByIdRequest(Guid Id) : IRequest;

public interface IGetTableByIdRequestHandler : IRequestHandler<GetTableByIdRequest, Table>;

public sealed class GetTableByIdRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetTableByIdRequestHandler> logger)
    : IGetTableByIdRequestHandler
{
    public async Task<IFinalResult<Table>> HandleAsync(GetTableByIdRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var table = await dbContext.Tables.AsNoTracking().Include(t => t.Attributes)
                .FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);

            return table is not null ? Result.Success(table) : Result.NotFound<Table>();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Table has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<Table>(ex, "Table could not been retrieved");
        }
    }
}