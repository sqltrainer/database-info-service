using Results;
using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

public sealed record GetAllTablesRequest(
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetAllTablesRequestHandler : IRequestHandler<GetAllTablesRequest, IReadOnlyCollection<Table>>;

public sealed class GetAllTablesRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetAllTablesRequestHandler> logger)
    : IGetAllTablesRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Table>>> HandleAsync(GetAllTablesRequest request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var query = dbContext.Tables.AsNoTracking();
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(t => t.Name.Contains(request.SearchTerm));

            var tables = await query.SortAndPaginate(request).ToListAsync(cancellationToken);
            
            return Result.Success(tables);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Tables has not been retrieved");
            return UnhandledFailedResult.Create<IReadOnlyCollection<Table>>(ex, "Tables could not been retrieved");
        }
    }
}