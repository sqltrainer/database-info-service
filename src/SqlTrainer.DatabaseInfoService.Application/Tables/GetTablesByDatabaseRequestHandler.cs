using Application;
using Application.Mediator.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

public record GetTablesByDatabaseRequest(
    Guid DatabaseId,
    string SearchTerm, 
    string OrderBy, 
    SortDirection OrderByDirection, 
    int Page, 
    int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);

public interface IGetTablesByDatabaseRequestHandler : IRequestHandler<GetTablesByDatabaseRequest, IReadOnlyCollection<Table>>;

public sealed class GetTablesByDatabaseRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetTablesByDatabaseRequestHandler> logger)
    : IGetTablesByDatabaseRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Table>>> HandleAsync(GetTablesByDatabaseRequest request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var query = dbContext.Tables.AsNoTracking().Where(t => t.DatabaseId == request.DatabaseId);
            
            if (!string.IsNullOrWhiteSpace(request.SearchTerm))
                query = query.Where(t => t.Name.Contains(request.SearchTerm));

            var tables = await query.SortAndPaginate(request).ToListAsync(cancellationToken);
            
            return Result.Success(tables);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Tables has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<Table>>(ex, "Tables could not been retrieved");
        }
    }
}