using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Helpers;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

using TableAttribute = Domain.Models.Attribute;

public sealed record UpdateTableRequest(
    Guid Id,
    string Name, 
    IReadOnlyCollection<UpdateTableAttributeRequest> Attributes) 
    : IRequest;

public sealed record UpdateTableAttributeRequest(
    Guid? Id,
    Guid TableId,
    string Name,
    string Type,
    int Order,
    bool IsPrimaryKey = false,
    bool IsNotNull = false,
    bool IsUnique = false,
    string? DefaultValue = null,
    int? VarcharNumberOfSymbols = null,
    Guid? ForeignKeyId = null)
{
    public UpdateTableAttributeRequest(TableAttribute attribute)
        : this(attribute.Id, attribute.TableId, attribute.Name, attribute.Type, attribute.Order, attribute.IsPrimaryKey,
            attribute.IsNotNull, attribute.IsUnique, attribute.DefaultValue, attribute.VarcharNumberOfSymbols,
            attribute.ForeignKeyId)
    {
    }
}

public interface IUpdateTableRequestHandler : IRequestHandler<UpdateTableRequest>;

public sealed class UpdateTableRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    IForeignKeyHelper foreignKeyHelper,
    ILogger<UpdateTableRequestHandler> logger)
    : IUpdateTableRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateTableRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = Validate(request);
            if (!validationResult.IsSucceeded)
                return validationResult;
            
            var newAttributes = request.Attributes.Where(a => a.Id is null).Select(ToModel).ToList();
            var existedAttributes = request.Attributes.Where(a => a.Id is not null).Select(ToModel).ToList();
            
            await foreignKeyHelper.SetForeignKeysAsync(newAttributes, cancellationToken);
            await foreignKeyHelper.SetForeignKeysAsync(existedAttributes, cancellationToken);

            var table = await dbContext.Tables.AsNoTracking().Include(t => t.Database).ThenInclude(d => d!.Language)
                .FirstOrDefaultAsync(t => t.Id == request.Id, cancellationToken);
            if (table is null)
                throw new ArgumentException($"Table with such id {request.Id} does not exist");
            
            var newTable = new Table(table.Id)
            {
                Name = request.Name,
                DatabaseId = table.DatabaseId
            }.WithDatabase(table.Database!).WithAttributes(newAttributes.Concat(existedAttributes));
            
            dbContext.Tables.Update(newTable);
            
            var tableAttributes = table.Attributes;
            
            var attributesToDelete = tableAttributes.ExceptBy(existedAttributes.Select(a => a.Id), a => a.Id);
            var attributesToUpdate = existedAttributes.ExceptBy(tableAttributes.Select(a => a.Id), a => a.Id);

            dbContext.Attributes.RemoveRange(attributesToDelete);
            dbContext.Attributes.UpdateRange(attributesToUpdate);
            dbContext.Attributes.AddRange(newAttributes);
            
            var alterResult = await databasesService.AlterTableAsync(table.Database!.Language!.ShortName, table.Database.ConnectionString, table, newTable, cancellationToken);
            if (!alterResult.IsSucceeded)
                return Result.Failed("Table could not be updated");
            
            await dbContext.SaveChangesAsync(cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Table could not be updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Table could not be updated");
        }
    }

    private static TableAttribute ToModel(UpdateTableAttributeRequest request) => new(request.Id ?? default)
    {
        Name = request.Name,
        Type = request.Type,
        IsNotNull = request.IsNotNull,
        IsUnique = request.IsUnique,
        DefaultValue = request.DefaultValue,
        VarcharNumberOfSymbols = request.VarcharNumberOfSymbols,
        IsPrimaryKey = request.IsPrimaryKey,
        ForeignKeyId = request.ForeignKeyId,
        TableId = request.TableId,
        Order = request.Order
    };
    
    public ValidationResult Validate(UpdateTableRequest request)
    {
        var validationResult = new ValidationResult();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Table name is required");

        var attributes = request.Attributes.ToArray();

        if (request.Attributes.Count == 0)
        {
            validationResult.Fail(nameof(request.Attributes), "Table must have at least one attribute");
            return validationResult;
        }

        foreach (var attribute in attributes)
        {
            if (string.IsNullOrWhiteSpace(attribute.Name))
                validationResult.Fail(nameof(attribute.Name), "Attribute name is required");
            
            if (string.IsNullOrWhiteSpace(attribute.Type))
                validationResult.Fail(nameof(attribute.Type), "Attribute type is required");
        }
        
        return validationResult;
    }
}