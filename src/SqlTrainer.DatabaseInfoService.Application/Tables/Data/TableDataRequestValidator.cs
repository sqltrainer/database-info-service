using Microsoft.EntityFrameworkCore;
using Results;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables.Data;

public interface ITableDataRequestValidator
{
    Task<IValidationFinalResult<Table>> ValidateAsync(Guid tableId, CancellationToken cancellationToken = default);
}

public sealed class TableDataRequestValidator(IDatabaseInfoDbContext dbContext) : ITableDataRequestValidator
{
    public async Task<IValidationFinalResult<Table>> ValidateAsync(Guid tableId, CancellationToken cancellationToken = default)
    {
        var validationResult = new ValidationResult<Table>();
        
        var table = await dbContext.Tables.AsNoTracking()
            .Include(t => t.Database)
            .ThenInclude(d => d!.Language)
            .FirstOrDefaultAsync(t => t.Id == tableId, cancellationToken);
        
        if (table is null)
            validationResult.Fail(nameof(tableId), "Table not found");
        else
            validationResult.Complete(table);
        
        return validationResult;
    }
}