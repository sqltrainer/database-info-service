using Results;
using Results.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Tables.Data;

public sealed record SelectDataRequest(Guid TableId) : IRequest;

public interface ISelectDataRequestHandler : IRequestHandler<SelectDataRequest, ScriptResult>;

public sealed class SelectDataRequestHandler(
    IDatabasesService databasesService,
    ITableDataRequestValidator validator,
    ILogger<SelectDataRequestHandler> logger)
    : ISelectDataRequestHandler
{
    public async Task<IFinalResult<ScriptResult>> HandleAsync(SelectDataRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await validator.ValidateAsync(request.TableId, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult.OfType<ScriptResult>();
            
            var table = validationResult.Value;
            
            var result = await databasesService.SelectDataAsync(table.Database!.Language!.ShortName, table.Database.ConnectionString, table, cancellationToken);
            
            return result;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Data could not be selected. Request: {Request}", request);
            return UnhandledFailedResult.Create<ScriptResult>(ex, "Data could not be selected");
        }
    }
}
