using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Tables.Data;

public sealed record InsertDataRequest(Guid TableId, string JsonData) : IRequest;

public interface IInsertDataRequestHandler : IRequestHandler<InsertDataRequest>;

public sealed class InsertDataRequestHandler(
    IDatabasesService databasesService,
    ITableDataRequestValidator validator,
    ILogger<InsertDataRequestHandler> logger)
    : IInsertDataRequestHandler
{
    public async Task<IFinalResult> HandleAsync(InsertDataRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await validator.ValidateAsync(request.TableId, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult;
            
            var table = validationResult.Value;
            
            await databasesService.InsertDataAsync(table.Database!.Language!.ShortName, table.Database.ConnectionString, table, request.JsonData, cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Data could not be inserted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Data could not be inserted");
        }
    }
}
