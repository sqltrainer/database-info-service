using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Tables.Data;

public sealed record UpdateDataRequest(Guid TableId, string OldJsonData, string NewJsonData) : IRequest;

public interface IUpdateDataRequestHandler : IRequestHandler<UpdateDataRequest>;

public sealed class UpdateDataRequestHandler(
    IDatabasesService databasesService,
    ITableDataRequestValidator validator,
    ILogger<UpdateDataRequestHandler> logger)
    : IUpdateDataRequestHandler
{
    public async Task<IFinalResult> HandleAsync(UpdateDataRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await validator.ValidateAsync(request.TableId, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult;
            
            var table = validationResult.Value;
            
            await databasesService.UpdateDataAsync(table.Database!.Language!.ShortName, table.Database.ConnectionString, table, request.OldJsonData, request.NewJsonData, cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Data could not be updated. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Data could not be updated");
        }
    }
}
