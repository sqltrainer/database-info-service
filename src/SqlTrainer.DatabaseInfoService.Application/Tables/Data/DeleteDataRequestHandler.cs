using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Tables.Data;

public sealed record DeleteDataRequest(Guid TableId, string JsonData) : IRequest;

public interface IDeleteDataRequestHandler : IRequestHandler<DeleteDataRequest>;

public sealed class DeleteDataRequestHandler(
    IDatabasesService databasesService,
    ITableDataRequestValidator validator,
    ILogger<DeleteDataRequestHandler> logger)
    : IDeleteDataRequestHandler
{
    public async Task<IFinalResult> HandleAsync(DeleteDataRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await validator.ValidateAsync(request.TableId, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult;
            
            var table = validationResult.Value;
            
            await databasesService.DeleteDataAsync(table.Database!.Language!.ShortName, table.Database.ConnectionString, table, request.JsonData, cancellationToken);
            
            return validationResult;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Data could not be deleted. Request: {Request}", request);
            return UnhandledFailedResult.Create(ex, "Data could not be deleted");
        }
    }
}
