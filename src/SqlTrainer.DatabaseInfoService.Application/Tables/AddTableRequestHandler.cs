using Results;
using Results.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Application.Helpers;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Tables;

using TableAttribute = Domain.Models.Attribute;

public sealed record AddTableRequest(
    string Name, 
    Guid DatabaseId,
    IEnumerable<AddTableAttributeRequest> Attributes) 
    : IRequest;
    
public sealed record AddTableAttributeRequest(
    string Name,
    string Type,
    bool IsPrimaryKey,
    bool IsNotNull,
    bool IsUnique,
    string? DefaultValue,
    int? VarcharNumberOfSymbols,
    Guid? ForeignKeyId,
    int Order);

public interface IAddTableRequestHandler : IRequestHandler<AddTableRequest, Guid>;

public sealed class AddTableRequestHandler(
    IDatabaseInfoDbContext dbContext,
    IDatabasesService databasesService,
    IForeignKeyHelper foreignKeyHelper,
    ILogger<AddTableRequestHandler> logger)
    : IAddTableRequestHandler
{
    public async Task<IFinalResult<Guid>> HandleAsync(AddTableRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await ValidateAsync(request, cancellationToken);
            if (!validationResult.IsSucceeded)
                return validationResult.OfType<Guid>();
            
            var table = new Table
            {
                Name = request.Name,
                DatabaseId = request.DatabaseId
            };
        
            var attributes = request.Attributes.Select(a => new TableAttribute
            {
                Name = a.Name,
                Type = a.Type,
                IsNotNull = a.IsNotNull,
                IsUnique = a.IsUnique,
                DefaultValue = a.DefaultValue,
                VarcharNumberOfSymbols = a.VarcharNumberOfSymbols,
                IsPrimaryKey = a.IsPrimaryKey,
                ForeignKeyId = a.ForeignKeyId,
                TableId = table.Id,
                Order = a.Order
            }).ToList();
        
            await foreignKeyHelper.SetForeignKeysAsync(attributes, cancellationToken);
        
            table.WithAttributes(attributes);
        
            var database = validationResult.Value;
            var createResult = await databasesService.CreateTableAsync(database.Language!.ShortName, database.ConnectionString, table, cancellationToken);
            if (!createResult.IsSucceeded)
            {
                logger.LogError("Table {Name} has not been added, because {Errors}", request.Name, string.Join("; ", createResult.Problems));
                return Result.Failed<Guid>("Could not create table");
            }
            
            await dbContext.Tables.AddAsync(table, cancellationToken);
            await dbContext.SaveChangesAsync(cancellationToken);

            return Result.Success(table.Id);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Table has not been added. Request: {Request}", request);
            return UnhandledFailedResult.Create<Guid>(ex, "Cannot add table.");
        }
    }
    
    private async Task<ValidationResult<Database>> ValidateAsync(AddTableRequest request, CancellationToken cancellationToken)
    {
        var validationResult = new ValidationResult<Database>();
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.Fail(nameof(request.Name), "Name is required");

        if (request.DatabaseId == Guid.Empty)
        {
            validationResult.Fail(nameof(request.DatabaseId), "DatabaseId is required");
            return validationResult;
        }
        
        var database = await dbContext.Databases.AsNoTracking()
            .Include(d => d.Language)
            .FirstOrDefaultAsync(d => d.Id == request.DatabaseId, cancellationToken);
        
        if (database is null)
            validationResult.Fail(nameof(request.DatabaseId), "Database not found");
        else
            validationResult.Complete(database);
        
        return validationResult;
    }
}