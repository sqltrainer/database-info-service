using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Languages;

public sealed record GetAllLanguagesRequest : IRequest;

public interface IGetAllLanguagesRequestHandler : IRequestHandler<GetAllLanguagesRequest, IReadOnlyCollection<Language>>;

public sealed class GetAllLanguagesRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetAllLanguagesRequestHandler> logger)
    : IGetAllLanguagesRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<Language>>> HandleAsync(GetAllLanguagesRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var languages = await dbContext.Languages.AsNoTracking().ToListAsync(cancellationToken);

            return Result.Success(languages);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Languages have not been retrieved");
            return UnhandledFailedResult.Create<IReadOnlyCollection<Language>>(ex, "Cannot retrieve languages.");
        }
    }
}