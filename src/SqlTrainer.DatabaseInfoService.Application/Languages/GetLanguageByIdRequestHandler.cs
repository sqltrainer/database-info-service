using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Languages;

public sealed record GetLanguageByIdRequest(Guid Id) : IRequest;

public interface IGetLanguageByIdRequestHandler : IRequestHandler<GetLanguageByIdRequest, Language>;

public sealed class GetLanguageByIdRequestHandler(
    IDatabaseInfoDbContext dbContext,
    ILogger<GetLanguageByIdRequestHandler> logger)
    : IGetLanguageByIdRequestHandler
{
    public async Task<IFinalResult<Language>> HandleAsync(GetLanguageByIdRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var language = await dbContext.Languages.AsNoTracking().FirstOrDefaultAsync(l => l.Id == request.Id, cancellationToken);
            
            return language is not null ? Result.Success(language) : Result.NotFound<Language>();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Language has not been retrieved. Request: {Request}", request);
            return UnhandledFailedResult.Create<Language>(ex, "Cannot retrieve language.");
        }
    }
}