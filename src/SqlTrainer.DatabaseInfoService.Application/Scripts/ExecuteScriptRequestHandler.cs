using Results;
using Results.Extensions;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Scripts;

public sealed record ExecuteScriptRequest(Guid DatabaseId, string Script) : IRequest;

public interface IExecuteScriptRequestHandler : IRequestHandler<ExecuteScriptRequest, ScriptResult>;

public sealed class ExecuteScriptRequestHandler(
    IScriptService scriptService,
    IScriptRequestValidator scriptRequestValidator,
    ILogger<ExecuteScriptRequestHandler> logger)
    : IExecuteScriptRequestHandler
{
    public async Task<IFinalResult<ScriptResult>> HandleAsync(ExecuteScriptRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await scriptRequestValidator.ValidateAsync(request.DatabaseId, nameof(request.DatabaseId), cancellationToken);
            if (validationResult.IsFailed)
                return validationResult.OfType<ScriptResult>();
            
            var database = validationResult.Value;
            
            return await scriptService.ExecuteAsync(database.Language!.ShortName, database.ConnectionString, request.Script, cancellationToken);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Script has not been executed. Request: {Request}", request);
            return UnhandledFailedResult.Create<ScriptResult>(ex, "Cannot execute script.");
        }
    }
}
