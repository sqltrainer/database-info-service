using Microsoft.EntityFrameworkCore;
using Results;
using SqlTrainer.DatabaseInfoService.Application.DAL;
using SqlTrainer.DatabaseInfoService.Domain.Models;

namespace SqlTrainer.DatabaseInfoService.Application.Scripts;

public interface IScriptRequestValidator
{
    Task<IValidationFinalResult<Database>> ValidateAsync(Guid databaseId, string propertyName, CancellationToken cancellationToken = default);
}

public sealed class ScriptRequestValidator(IDatabaseInfoDbContext dbContext) : IScriptRequestValidator
{
    public async Task<IValidationFinalResult<Database>> ValidateAsync(Guid databaseId, string propertyName, CancellationToken cancellationToken = default)
    {
        var validationResult = new ValidationResult<Database>();
        if (databaseId == Guid.Empty)
        {
            validationResult.Fail(propertyName, "Database Id is empty");
            return validationResult;
        }

        var database = await dbContext.Databases.AsNoTracking().Include(d => d.Language).FirstOrDefaultAsync(d => d.Id == databaseId, cancellationToken);
        if (database is null)
            validationResult.Fail(propertyName, "Database not found");
        else
            validationResult.Complete(database);

        return validationResult;
    }
}