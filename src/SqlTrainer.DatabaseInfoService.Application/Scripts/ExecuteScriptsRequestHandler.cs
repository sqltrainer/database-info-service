using Results;
using Application.Mediator.Handlers;
using Application.Mediator.Requests;
using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using Results.Extensions;
using SqlTrainer.DatabaseInfoService.Application.Services;

namespace SqlTrainer.DatabaseInfoService.Application.Scripts;

public sealed record ExecuteScriptsRequest(Guid DatabaseId, IEnumerable<string> Scripts) : IRequest;

public interface IExecuteScriptsRequestHandler : IRequestHandler<ExecuteScriptsRequest, IReadOnlyCollection<ScriptResult>>;

public sealed class ExecuteScriptsRequestHandler(
    IScriptService scriptService,
    IScriptRequestValidator scriptRequestValidator,
    ILogger<ExecuteScriptsRequestHandler> logger)
    : IExecuteScriptsRequestHandler
{
    public async Task<IFinalResult<IReadOnlyCollection<ScriptResult>>> HandleAsync(ExecuteScriptsRequest request, CancellationToken cancellationToken = default)
    {
        try
        {
            var validationResult = await scriptRequestValidator.ValidateAsync(request.DatabaseId, nameof(request.DatabaseId), cancellationToken);
            if (validationResult.IsFailed)
                return validationResult.OfType<IReadOnlyCollection<ScriptResult>>();
            
            var database = validationResult.Value;
            
            return await scriptService.ExecuteAsync(database.Language!.ShortName, database.ConnectionString, request.Scripts, cancellationToken);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Scripts have not been executed. Request: {Request}", request);
            return UnhandledFailedResult.Create<IReadOnlyCollection<ScriptResult>>(ex, "Cannot execute scripts.");
        }
    }
}
