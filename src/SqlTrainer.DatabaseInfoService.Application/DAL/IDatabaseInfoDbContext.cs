using Application;
using Microsoft.EntityFrameworkCore;
using SqlTrainer.DatabaseInfoService.Domain.Models;

using TableAttribute = SqlTrainer.DatabaseInfoService.Domain.Models.Attribute;

namespace SqlTrainer.DatabaseInfoService.Application.DAL;

public interface IDatabaseInfoDbContext : IApplicationDbContext
{
    public DbSet<Language> Languages { get; set; }
    public DbSet<Database> Databases { get; set; }
    public DbSet<Table> Tables { get; set; }
    public DbSet<TableAttribute> Attributes { get; set; }
}