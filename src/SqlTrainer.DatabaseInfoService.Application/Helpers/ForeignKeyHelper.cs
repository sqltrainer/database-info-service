using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.DAL;

namespace SqlTrainer.DatabaseInfoService.Application.Helpers;

using TableAttribute = Domain.Models.Attribute;

public interface IForeignKeyHelper
{
    Task SetForeignKeysAsync(IEnumerable<TableAttribute> attributes, CancellationToken cancellationToken = default);
}

public sealed class ForeignKeyHelper(
    IDatabaseInfoDbContext dbContext,
    ILogger<ForeignKeyHelper> logger)
    : IForeignKeyHelper
{
    public async Task SetForeignKeysAsync(IEnumerable<TableAttribute> attributes, CancellationToken cancellationToken = default)
    {
        foreach (var attribute in attributes)
        {
            if (attribute.ForeignKeyId is null)
                continue;

            await SetForeignKeyAsync(attribute, cancellationToken);

            if (cancellationToken.IsCancellationRequested)
                return;
        }
    }

    private async Task SetForeignKeyAsync(TableAttribute attribute, CancellationToken cancellationToken)
    {
        var foreignKey = await GetForeignKeyAsync(attribute.ForeignKeyId!.Value, cancellationToken);
        if (foreignKey is null)
            return;

        attribute.WithForeignKey(foreignKey);
    }

    private async Task<TableAttribute?> GetForeignKeyAsync(Guid foreignKeyId, CancellationToken cancellationToken)
    {
        var foreignKey = await dbContext.Attributes
            .Include(a => a.Table)
            .FirstOrDefaultAsync(a => a.Id == foreignKeyId, cancellationToken);
        
        if (cancellationToken.IsCancellationRequested)
            return null;

        if (foreignKey is not null) 
            return foreignKey;
        
        logger.LogWarning("Foreign key {ForeignKeyId} not found", foreignKeyId);
        return null;
    }
}