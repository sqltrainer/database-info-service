using Application;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.DatabaseInfoService.Application.Helpers;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Application.Tables;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.IntegrationTests.DAL;

namespace SqlTrainer.DatabaseInfoService.IntegrationTests.Tables;

public class TableServiceTests
{
    private static readonly Mock<IDatabasesService> DatabasesServiceMock = new();
    private static readonly Mock<ILogger<ForeignKeyHelper>> ForeignKeyHelperLoggerMock = new();
    
    public class AddTests : TableServiceTests
    {
        private const string DbContextName = "AddTableDatabase";
        
        private static readonly Mock<ILogger<AddTableRequestHandler>> LoggerMock = new();
        
        private readonly AddTableRequestHandler sut;
        
        private readonly Table mssqlTable;
        private readonly Table postgresTable;

        public AddTests()
        {
            DatabasesServiceMock.Setup(s => s.CreateTableAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Table>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            
            var foreignKeyHelper = new ForeignKeyHelper(DatabaseInfoInMemoryDbContext.Create(DbContextName), ForeignKeyHelperLoggerMock.Object);
            sut = new AddTableRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabasesServiceMock.Object, foreignKeyHelper, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);

            Language mssqlLanguage = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Language postgresLanguage = new() { Name = "PostgreSQL", ShortName = "postgres", BaseConnectionString = string.Empty };
            
            Database mssqlDatabase = new() { Name = "AddTable Test MSSQL", ConnectionString = string.Empty, LanguageId = Guid.Empty };
            Database postgresDatabase = new() { Name = "AddTable Test Postgres", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            mssqlDatabase.WithLanguage(mssqlLanguage);
            postgresDatabase.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange(mssqlDatabase, postgresDatabase);
            
            mssqlTable = new Table { Name = "AddTable Test MSSQL", DatabaseId = mssqlDatabase.Id };
            postgresTable = new Table { Name = "AddTable Test Postgres", DatabaseId = postgresDatabase.Id };
            
            var mssqlAttributes = new[]
            {
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            };
            
            var postgresAttributes = new[]
            {
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            };
            
            mssqlTable.WithAttributes(mssqlAttributes);
            postgresTable.WithAttributes(postgresAttributes);

            dbContext.SaveChanges();
        }
        
        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Add_ReturnResponse(string language)
        {
            // Arrange
            var expectedTable = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language.")
            };
            var attributes = expectedTable.Attributes
                .Select(a => new AddTableAttributeRequest(
                    a.Name, a.Type, a.IsPrimaryKey, a.IsNotNull, a.IsUnique, a.DefaultValue, a.VarcharNumberOfSymbols, a.ForeignKeyId, a.Order)
                );
            
            var request = new AddTableRequest(expectedTable.Name, expectedTable.DatabaseId, attributes);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            var table = await dbContext.Tables.Include(t => t.Attributes).FirstOrDefaultAsync(t => t.Id == result.Value);
            table.Should().NotBeNull();
            table!.Name.Should().Be(expectedTable.Name);
            table.Attributes.Should().NotBeNullOrEmpty();
            table.Attributes.Should().HaveCount(expectedTable.Attributes.Count);
        }
    }

    public class UpdateTests : TableServiceTests
    {
        private const string DbContextName = "UpdateTableDatabase";
        
        private static readonly Mock<ILogger<UpdateTableRequestHandler>> LoggerMock = new();
        
        private readonly UpdateTableRequestHandler sut;
        
        private readonly Table mssqlTable;
        private readonly Table postgresTable;
        
        public UpdateTests()
        {
            DatabasesServiceMock.Setup(s => s.AlterTableAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Table>(), It.IsAny<Table>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            
            var foreignKeyHelper = new ForeignKeyHelper(DatabaseInfoInMemoryDbContext.Create(DbContextName), ForeignKeyHelperLoggerMock.Object);
            sut = new UpdateTableRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabasesServiceMock.Object, foreignKeyHelper, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            Language mssqlLanguage = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Language postgresLanguage = new() { Name = "PostgreSQL", ShortName = "postgres", BaseConnectionString = string.Empty };
            
            Database mssqlDatabase = new() { Name = "UpdateTable Test MSSQL", ConnectionString = string.Empty, LanguageId = Guid.Empty };
            Database postgresDatabase = new() { Name = "UpdateTable Test Postgres", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            mssqlDatabase.WithLanguage(mssqlLanguage);
            postgresDatabase.WithLanguage(postgresLanguage);
            
            mssqlTable = new Table { Name = "UpdateTable Test MSSQL", DatabaseId = mssqlDatabase.Id }.WithDatabase(mssqlDatabase);
            postgresTable = new Table { Name = "UpdateTable Test Postgres", DatabaseId = postgresDatabase.Id }.WithDatabase(postgresDatabase);
            
            Domain.Models.Attribute[] mssqlAttributes =
            [
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            ];
            
            Domain.Models.Attribute[] postgresAttributes =
            [
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            ];
            
            mssqlTable.WithAttributes(mssqlAttributes);
            postgresTable.WithAttributes(postgresAttributes);
            
            dbContext.Tables.AddRange([mssqlTable, postgresTable]);
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Update_ReturnResponse(string language)
        {
            // Arrange
            var expectedTable = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language.")
            };

            var pkAttribute = expectedTable.Attributes.FirstOrDefault(a => a.IsPrimaryKey);
            UpdateTableAttributeRequest? pkAttributeRequest = null;
            if (pkAttribute is not null)
                pkAttributeRequest = new UpdateTableAttributeRequest(pkAttribute);
                
            var notPkAttribute = expectedTable.Attributes.FirstOrDefault(a => !a.IsPrimaryKey);
            UpdateTableAttributeRequest? updatedAttributeRequest = null;
            if (notPkAttribute is not null)
                updatedAttributeRequest = new UpdateTableAttributeRequest(notPkAttribute.Id, expectedTable.Id, notPkAttribute.Name, notPkAttribute.Type, notPkAttribute.Order, VarcharNumberOfSymbols: 50, DefaultValue: "unknown");

            var newAttributeRequest = new UpdateTableAttributeRequest(null, expectedTable.Id, "createdAt", "datetime", 3, IsNotNull: true);
            
            var expectedTableName = expectedTable.Name + "Updated";
            UpdateTableAttributeRequest?[] attributes = [pkAttributeRequest, updatedAttributeRequest, newAttributeRequest];
            
            var request = new UpdateTableRequest(expectedTable.Id, expectedTableName, attributes.Where(a => a is not null).Select(a => a!).ToArray());

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            var actualTable = await dbContext.Tables.Include(t => t.Attributes).FirstOrDefaultAsync(t => t.Id == expectedTable.Id);
            actualTable.Should().NotBeNull();
            actualTable!.Name.Should().Be(expectedTableName);
            actualTable.Attributes.Should().NotBeNullOrEmpty();
            actualTable.Attributes.Should().HaveCount(request.Attributes.Count);
            var actualAttributes = actualTable.Attributes.OrderBy(a => a.Order).ToArray();
            var expectedAttributes = request.Attributes.OrderBy(a => a.Order).ToArray();
            actualAttributes.Should().HaveCount(expectedAttributes.Length);
            for (var i = 0; i < expectedAttributes.Length; i++)
            {
                actualAttributes[i].Name.Should().Be(expectedAttributes[i].Name);
                actualAttributes[i].Type.Should().Be(expectedAttributes[i].Type);
                actualAttributes[i].IsNotNull.Should().Be(expectedAttributes[i].IsNotNull);
                actualAttributes[i].IsUnique.Should().Be(expectedAttributes[i].IsUnique);
                actualAttributes[i].IsPrimaryKey.Should().Be(expectedAttributes[i].IsPrimaryKey);
                actualAttributes[i].DefaultValue.Should().Be(expectedAttributes[i].DefaultValue);
                actualAttributes[i].VarcharNumberOfSymbols.Should().Be(expectedAttributes[i].VarcharNumberOfSymbols);
                actualAttributes[i].ForeignKeyId.Should().Be(expectedAttributes[i].ForeignKeyId);
                actualAttributes[i].Order.Should().Be(expectedAttributes[i].Order);
            }
        }
    }

    public class DeleteTests : TableServiceTests
    {
        private const string DbContextName = "DeleteTableDatabase";
        
        private static readonly Mock<ILogger<DeleteTableRequestHandler>> LoggerMock = new();
        
        private readonly DeleteTableRequestHandler sut;
        
        private readonly Table mssqlTable;
        private readonly Table postgresTable;
        
        public DeleteTests()
        {
            DatabasesServiceMock.Setup(s => s.DropTableAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            
            sut = new DeleteTableRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabasesServiceMock.Object, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            Language mssqlLanguage = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Language postgresLanguage = new() { Name = "PostgreSQL", ShortName = "postgres", BaseConnectionString = string.Empty };
            
            Database mssqlDatabase = new() { Name = "DeleteTable Test MSSQL", ConnectionString = string.Empty, LanguageId = Guid.Empty };
            Database postgresDatabase = new() { Name = "DeleteTable Test Postgres", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            mssqlDatabase.WithLanguage(mssqlLanguage);
            postgresDatabase.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange([mssqlDatabase, postgresDatabase]);
            
            mssqlTable = new Table { Name = "DeleteTable Test MSSQL", DatabaseId = mssqlDatabase.Id };
            postgresTable = new Table { Name = "DeleteTable Test Postgres", DatabaseId = postgresDatabase.Id };
            
            dbContext.Tables.AddRange([mssqlTable, postgresTable]);
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Delete_ReturnResponse(string language)
        {
            // Arrange
            var expectedTable = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language.")
            };
            var request = new DeleteTableRequest(expectedTable.Id);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            var tables = await dbContext.Tables.FindAsync(expectedTable.DatabaseId);
            tables.Should().BeNull();
        }
    }

    public class GetTests : TableServiceTests
    {
        private const string DbContextName = "GetTableDatabase";
        
        private static readonly Mock<ILogger<GetTableByIdRequestHandler>> LoggerMock = new();
        
        private readonly GetTableByIdRequestHandler sut;
        
        private readonly Table mssqlTable;
        private readonly Table postgresTable;
        
        public GetTests()
        {
            sut = new GetTableByIdRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            Language mssqlLanguage = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Language postgresLanguage = new() { Name = "PostgreSQL", ShortName = "postgres", BaseConnectionString = string.Empty };
            
            Database mssqlDatabase = new() { Name = "GetTable Test MSSQL", ConnectionString = string.Empty, LanguageId = Guid.Empty };
            Database postgresDatabase = new() { Name = "GetTable Test Postgres", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            mssqlDatabase.WithLanguage(mssqlLanguage);
            postgresDatabase.WithLanguage(postgresLanguage);
            
            mssqlTable = new Table { Name = "GetTable Test MSSQL", DatabaseId = mssqlDatabase.Id };
            postgresTable = new Table { Name = "GetTable Test Postgres", DatabaseId = postgresDatabase.Id };
            
            Domain.Models.Attribute[] mssqlAttributes =
            [
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = mssqlTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            ];
            
            Domain.Models.Attribute[] postgresAttributes =
            [
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                new Domain.Models.Attribute { TableId = postgresTable.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
            ];
            
            mssqlTable.WithAttributes(mssqlAttributes);
            postgresTable.WithAttributes(postgresAttributes);
            
            dbContext.Tables.AddRange([mssqlTable, postgresTable]);
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Get_ReturnGetTableResponse(string language)
        {
            // Arrange
            var expectedTable = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language.")
            };
            var request = new GetTableByIdRequest(expectedTable.Id);
            
            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            result.Value.Should().NotBeNull();
            result.Value.Id.Should().Be(expectedTable.Id.ToString());
            result.Value.Name.Should().Be(expectedTable.Name);
            result.Value.Attributes.Should().NotBeNullOrEmpty();
            result.Value.Attributes.Should().HaveCount(expectedTable.Attributes.Count);
        }
    }

    public class GetAllTests : TableServiceTests
    {
        private const string DbContextName = "GetAllTablesDatabase";
        
        private static readonly Mock<ILogger<GetAllTablesRequestHandler>> LoggerMock = new();
        
        private readonly GetAllTablesRequestHandler sut;
        
        private readonly IReadOnlyCollection<Table> tables;

        public GetAllTests()
        {
            sut = new GetAllTablesRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName),
                LoggerMock.Object);

            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);

            Language language = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Database database = new() { Name = "Get Tables Test", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            database.WithLanguage(language);
            
            tables = Enumerable.Range(1, 500).Select(i =>
            {
                var table = new Table { Name = $"Get Tables {i.ToString().PadLeft(3, '0')}", DatabaseId = database.Id };
                table.WithDatabase(database);
                
                var attributes = new[]
                {
                    new Domain.Models.Attribute { TableId = table.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                    new Domain.Models.Attribute { TableId = table.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
                };
                
                table.WithAttributes(attributes);
                
                return table;
            }).ToArray();
            
            dbContext.Tables.AddRange(tables);
            
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task GetAll_ReturnGetTestsResponse()
        {
            // Arrange
            var request = new GetAllTablesRequest("5", "name", SortDirection.Ascending, 3, 30);

            // Act
            var result = await sut.HandleAsync(request);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            result.Value.Should().NotBeNullOrEmpty();
            result.Value.Should().HaveCount(30);
            var expectedTables = tables
                .Where(d => d.Name.Contains('5'))
                .OrderBy(d => d.Name)
                .Skip(60)
                .Take(30);
            result.Value.Should().BeEquivalentTo(expectedTables);
        }
    }

    public class GetByDatabaseTests : TableServiceTests
    {
        private const string DbContextName = "GetTablesByDatabaseDatabase";
        
        private static readonly Mock<ILogger<GetTablesByDatabaseRequestHandler>> LoggerMock = new();
        
        private readonly GetTablesByDatabaseRequestHandler sut;
        
        private readonly Database mssqlDatabase;
        private readonly Database postgresDatabase;
        
        private readonly IReadOnlyCollection<Table> tables;
        
        public GetByDatabaseTests()
        {
            sut = new GetTablesByDatabaseRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            Language mssqlLanguage = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = string.Empty };
            Language postgresLanguage = new() { Name = "PostgreSQL", ShortName = "postgres", BaseConnectionString = string.Empty };
            
            mssqlDatabase = new Database { Name = "GetTablesByDatabase Test MSSQL", ConnectionString = string.Empty, LanguageId = Guid.Empty };
            postgresDatabase = new Database { Name = "GetTablesByDatabase Test Postgres", ConnectionString = string.Empty, LanguageId = Guid.Empty };

            mssqlDatabase.WithLanguage(mssqlLanguage);
            postgresDatabase.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange([mssqlDatabase, postgresDatabase]);
            
            tables = Enumerable.Range(1, 500).Select(i =>
            {
                var table = new Table { Name = $"GetTablesByDatabase {i.ToString().PadLeft(3, '0')}", DatabaseId = i % 2 == 0 ? mssqlDatabase.Id : postgresDatabase.Id };
                table.WithDatabase(i % 2 == 0 ? mssqlDatabase : postgresDatabase);
                
                var attributes = new[]
                {
                    new Domain.Models.Attribute { TableId = table.Id, Name = "id", Type = "int", IsPrimaryKey = true, IsNotNull = true, IsUnique = false, Order = 1 },
                    new Domain.Models.Attribute { TableId = table.Id, Name = "name", Type = "varchar", IsPrimaryKey = false, IsNotNull = true, IsUnique = true, Order = 2, VarcharNumberOfSymbols = 50, DefaultValue = "unknown" }
                };
                
                table.WithAttributes(attributes);
                
                return table;
            }).ToArray();
            
            dbContext.Tables.AddRange(tables);
            
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task GetByDatabase_ReturnGetTablesResponse(string language)
        {
            // Arrange
            var database = language switch
            {
                "MSSQL" => mssqlDatabase,
                "Postgres" => postgresDatabase,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language.")
            };
            var request = new GetTablesByDatabaseRequest(database.Id, string.Empty, "name", SortDirection.Descending, 2, 50);
            
            // Act
            var response = await sut.HandleAsync(request);
            
            // Assert
            response.IsSucceeded.Should().BeTrue(string.Join(", ", response.Problems));
            response.Problems.Should().BeEmpty();
            response.Value.Should().NotBeNullOrEmpty();
            var expectedTables = tables
                .Where(d => d.DatabaseId == database.Id)
                .OrderByDescending(d => d.Name)
                .Skip(50)
                .Take(50)
                .ToArray();
            response.Value.Should().BeEquivalentTo(expectedTables);
        }
    }
}