using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Languages;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.IntegrationTests.DAL;

namespace SqlTrainer.DatabaseInfoService.IntegrationTests.Languages;

public class LanguageServiceTests
{
    private readonly IReadOnlyCollection<Language> languages;
    
    protected LanguageServiceTests(string dbContextName)
    {
        using var context = DatabaseInfoInMemoryDbContext.Create(dbContextName);

        var mssqlLanguage = new Language
        {
            Name = "Microsoft SQL Server",
            ShortName = "MSSQL",
            BaseConnectionString = "Data Source={Server};Initial Catalog={Database};User ID={User};Password={Password};"
        };
        
        var postgresLanguage = new Language
        {
            Name = "Postgres",
            ShortName = "PostgreSQL",
            BaseConnectionString = "Server={Server};Port={Port};User Id={User};Password={Password};Database={Database};"
        };
        
        languages = [mssqlLanguage, postgresLanguage];
        
        context.Languages.AddRange(languages);
        context.SaveChanges();
    }
    
    public class GetTests() : LanguageServiceTests(DbContextName)
    {
        private const string DbContextName = "GetLanguageById";
        
        private static readonly Mock<ILogger<GetLanguageByIdRequestHandler>> LoggerMock = new();
        private readonly GetLanguageByIdRequestHandler sut = new(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
        
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task Get_Test_ReturnGetLanguageResponse(int languageIndex)
        {
            // Arrange
            var language = languages.ElementAt(languageIndex);
            var getLanguageByIdRequest = new GetLanguageByIdRequest(language.Id);

            // Act
            var result = await sut.HandleAsync(getLanguageByIdRequest);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeNull();
            result.Value.Id.Should().Be(language.Id);
            result.Value.Name.Should().Be(language.Name);
            result.Value.ShortName.Should().Be(language.ShortName);
        }
    }

    public class GetAllTests() : LanguageServiceTests(DbContextName)
    {
        private const string DbContextName = "GetAllLanguages";
        
        private  static readonly Mock<ILogger<GetAllLanguagesRequestHandler>> LoggerMock = new();
        private readonly GetAllLanguagesRequestHandler sut = new(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
        
        [Fact]
        public async Task GetAll_Success_ReturnGetLanguagesResponse()
        {
            // Arrange
            var getAllLanguagesRequest = new GetAllLanguagesRequest();
            
            // Act
            var result = await sut.HandleAsync(getAllLanguagesRequest);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeEmpty();
            result.Value.Should().HaveCount(2);
            result.Value.Should().BeEquivalentTo(languages);
        }
    }
}