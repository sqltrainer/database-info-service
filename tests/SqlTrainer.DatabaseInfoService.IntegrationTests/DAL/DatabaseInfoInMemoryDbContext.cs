using System.Collections.Concurrent;
using Microsoft.EntityFrameworkCore;
using SqlTrainer.DatabaseInfoService.Persistence.Data;

namespace SqlTrainer.DatabaseInfoService.IntegrationTests.DAL;

public class DatabaseInfoInMemoryDbContext(DbContextOptions<DatabaseInfoDbContext> options) 
    : DatabaseInfoDbContext(options)
{
    private static readonly ConcurrentDictionary<string, DbContextOptions<DatabaseInfoDbContext>> Options = [];
    
    public static DatabaseInfoDbContext Create(string key, Action<DatabaseInfoDbContext>? seed = null)
    {
        if (Options.TryGetValue(key, out var options))
            return new DatabaseInfoDbContext(options);
        
        options = new DbContextOptionsBuilder<DatabaseInfoDbContext>()
            .UseInMemoryDatabase(key)
            .Options;
        
        var dbContext = new DatabaseInfoDbContext(options);

        seed?.Invoke(dbContext);

        dbContext.SaveChanges();

        Options.TryAdd(key, options);
        
        return dbContext;
    }
}