using Application;
using Microsoft.Extensions.Logging;
using Results;
using SqlTrainer.DatabaseInfoService.Application.Configurations;
using SqlTrainer.DatabaseInfoService.Application.Databases;
using SqlTrainer.DatabaseInfoService.Application.Services;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.IntegrationTests.DAL;

namespace SqlTrainer.DatabaseInfoService.IntegrationTests.Databases;

public class DatabaseServiceTests
{
    private static readonly Mock<IDatabasesService> DatabaseServiceMock = new();

    private static readonly LanguagesConfigurations LanguagesConfigurations = new()
    {
        Languages = new Dictionary<string, LanguageSettings>()
        {
            {
                "MSSQL", new LanguageSettings
                {
                    Server = "localhost",
                    Port = 1433,
                    Database = "master",
                    User = "sa",
                    Password = "password"
                }
            },
            {
                "Postgres", new LanguageSettings
                {
                    Server = "localhost",
                    Port = 5432,
                    Database = "postgres",
                    User = "postgres",
                    Password = "password"
                }
            }
        }
    };
    
    public DatabaseServiceTests()
    {
        DatabaseServiceMock.Setup(d => d.CheckConnectionAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(Result.Success());
    }
    
    public class AddTests : DatabaseServiceTests
    {
        private const string DbContextName = "AddDatabases";
        
        private static readonly Mock<ILogger<AddDatabaseRequestHandler>> LoggerMock = new();
        
        private readonly IAddDatabaseRequestHandler sut;
        
        private readonly Database mssqlDatabase;
        private readonly Database postgresDatabase;

        public AddTests()
        {
            DatabaseServiceMock.Setup(d => d.CreateDatabaseAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            
            sut = new AddDatabaseRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabaseServiceMock.Object, LanguagesConfigurations, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);

            var mssqlLanguage = new Language { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
            var postgresLanguage = new Language { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
            
            dbContext.Languages.AddRange(mssqlLanguage, postgresLanguage);
            dbContext.SaveChanges();
            
            mssqlDatabase = new Database { Name = "MSSQL Database", LanguageId = mssqlLanguage.Id, ConnectionString = "" }.WithLanguage(mssqlLanguage);
            postgresDatabase = new Database { Name = "Postgres Database", LanguageId = postgresLanguage.Id, ConnectionString = "" }.WithLanguage(postgresLanguage);
        }
        
        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Add_ReturnResponse(string language)
        {
            // Arrange
            var expectedDatabase = language switch
            {
                "MSSQL" => mssqlDatabase,
                "Postgres" => postgresDatabase,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language")
            };
            var request = new AddDatabaseRequest(expectedDatabase.Name, expectedDatabase.LanguageId);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            var database = await dbContext.Databases.FindAsync(result.Value);
            database.Should().NotBeNull();
            database!.Name.Should().Be(expectedDatabase.Name);
            database.LanguageId.Should().Be(expectedDatabase.LanguageId);
        }
    }

    public class UpdateTests : DatabaseServiceTests
    {
        private const string DbContextName = "UpdateDatabases";
        
        private static readonly Mock<ILogger<UpdateDatabaseRequestHandler>> LoggerMock = new();
        
        private readonly UpdateDatabaseRequestHandler sut;
        
        private readonly Database mssqlDatabase;
        private readonly Database postgresDatabase;

        public UpdateTests()
        {
            DatabaseServiceMock.Setup(s => s.RenameDatabaseAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            DatabaseServiceMock.Setup(s => s.AlterTableAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Table>(), It.IsAny<Table>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());
            
            sut = new UpdateDatabaseRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabaseServiceMock.Object, LanguagesConfigurations, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            var mssqlLanguage = new Language { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
            var postgresLanguage = new Language { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
            
            mssqlDatabase = new Database { Name = "MSSQL Database", LanguageId = mssqlLanguage.Id, ConnectionString = "" }.WithLanguage(mssqlLanguage);
            postgresDatabase = new Database { Name = "Postgres Database", LanguageId = postgresLanguage.Id, ConnectionString = "" }.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange(mssqlDatabase, postgresDatabase);
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Update_ReturnResponse(string language)
        {
            // Arrange
            var expectedDatabase = language switch
            {
                "MSSQL" => mssqlDatabase,
                "Postgres" => postgresDatabase,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language")
            };
            var request = new UpdateDatabaseRequest(expectedDatabase.Id, $"Database Update New Name - {Guid.NewGuid()}");

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            var database = await dbContext.FindAsync<Database>(expectedDatabase.Id);
            database.Should().NotBeNull();
            database!.Id.Should().Be(expectedDatabase.Id);
            database.Name.Should().Be(request.Name);
            database.Name.Should().NotBe(expectedDatabase.Name);
            database.LanguageId.Should().Be(expectedDatabase.LanguageId);
        }
    }
    
    public class DeleteTests : DatabaseServiceTests
    {
        private const string DbContextName = "DeleteDatabases";
        
        private static readonly Mock<ILogger<DeleteDatabaseRequestHandler>> LoggerMock = new();
        
        private readonly DeleteDatabaseRequestHandler sut;
        
        private readonly Database mssqlDatabase;
        private readonly Database postgresDatabase;

        public DeleteTests()
        {
            DatabaseServiceMock.Setup(s => s.DropDatabaseAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(Result.Success());

            sut = new DeleteDatabaseRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), DatabaseServiceMock.Object, LanguagesConfigurations, LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            var mssqlLanguage = new Language { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
            var postgresLanguage = new Language { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
            
            mssqlDatabase = new Database { Name = "MSSQL Database", LanguageId = mssqlLanguage.Id, ConnectionString = "" }.WithLanguage(mssqlLanguage);
            postgresDatabase = new Database { Name = "Postgres Database", LanguageId = postgresLanguage.Id, ConnectionString = "" }.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange(mssqlDatabase, postgresDatabase);
            dbContext.SaveChanges();
        }
        
        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task Delete_ReturnResponse(string language)
        {
            // Arrange
            var id = language switch
            {
                "MSSQL" => mssqlDatabase.Id,
                "Postgres" => postgresDatabase.Id,
                _ => throw new ArgumentOutOfRangeException(nameof(language), "Invalid language")
            };
            var request = new DeleteDatabaseRequest(id);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            
            await using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            var database = await dbContext.FindAsync<Database>(id);
            database.Should().BeNull();
        }
    }

    public class GetTests : DatabaseServiceTests
    {
        private const string DbContextName = "GetDatabases";
        
        private static readonly Mock<ILogger<GetDatabaseByIdRequestHandler>> LoggerMock = new();
        
        private readonly GetDatabaseByIdRequestHandler sut;
        
        private readonly Database mssqlDatabase;
        private readonly Database postgresDatabase;
        
        public GetTests()
        {
            sut = new GetDatabaseByIdRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            var mssqlLanguage = new Language { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
            var postgresLanguage = new Language { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
            
            mssqlDatabase = new Database { Name = "MSSQL Database", LanguageId = mssqlLanguage.Id, ConnectionString = "" }.WithLanguage(mssqlLanguage);
            postgresDatabase = new Database { Name = "Postgres Database", LanguageId = postgresLanguage.Id, ConnectionString = "" }.WithLanguage(postgresLanguage);
            
            dbContext.Databases.AddRange(mssqlDatabase, postgresDatabase);
            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task GetById_ReturnResponse(string language)
        {
            // Arrange
            var expectedDatabase = language switch
            {
                "MSSQL" => mssqlDatabase,
                "Postgres" => postgresDatabase,
                _ => throw new ArgumentException("Invalid language", nameof(language))
            };
            var request = new GetDatabaseByIdRequest(expectedDatabase.Id);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            result.Value.Should().NotBeNull();
            result.Value.Id.Should().Be(expectedDatabase.Id);
            result.Value.Name.Should().Be(expectedDatabase.Name);
            result.Value.LanguageId.Should().Be(expectedDatabase.LanguageId);
        }
    }

    public class GetAllTests : DatabaseServiceTests
    {
        private const string DbContextName = "GetAllDatabases";
        
        private static readonly Language MsSql = new() { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
        private static readonly Language Postgres = new() { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
        
        private static readonly IReadOnlyCollection<Database> Databases = Enumerable.Range(1, 500).Select(i => new Database
        {
            Name = $"Get All Databases Test {i.ToString().PadLeft(3, '0')}",
            LanguageId = Guid.Empty,
            ConnectionString = ""
        }.WithLanguage(i % 2 == 0 ? MsSql : Postgres)).ToArray();
        
        private static readonly Mock<ILogger<GetAllDatabasesRequestHandler>> LoggerMock = new();
        
        private readonly GetAllDatabasesRequestHandler sut;
        
        public GetAllTests()
        {
            sut = new GetAllDatabasesRequestHandler(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
            
            using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
            
            dbContext.Languages.AddRange(MsSql, Postgres);
            dbContext.Databases.AddRange(Databases);
            dbContext.SaveChanges();
        }

        [Fact]
        public async Task GetAll_ReturnGetDatabasesResponse()
        {
            // Arrange
            var request = new GetAllDatabasesRequest("6", "name", SortDirection.Descending, 1, 10);

            // Act
            var result = await sut.HandleAsync(request);

            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Problems.Should().BeEmpty();
            result.Value.Should().NotBeNull();
            result.Value.Should().HaveCount(10);
            foreach (var database in Databases.Where(d => d.Name.Contains('6')).OrderByDescending(d => d.Name).Take(10))
            {
                result.Value.Should().Contain(d => 
                    d.Id == database.Id && 
                    d.Name.Equals(database.Name) && 
                    d.LanguageId == database.LanguageId);
            }
        }
    }
}