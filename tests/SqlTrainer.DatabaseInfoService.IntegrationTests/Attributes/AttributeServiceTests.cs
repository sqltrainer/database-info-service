using Microsoft.Extensions.Logging;
using SqlTrainer.DatabaseInfoService.Application.Attributes;
using SqlTrainer.DatabaseInfoService.Domain.Models;
using SqlTrainer.DatabaseInfoService.IntegrationTests.DAL;
using Attribute = SqlTrainer.DatabaseInfoService.Domain.Models.Attribute;

namespace SqlTrainer.DatabaseInfoService.IntegrationTests.Attributes;

public class AttributeServiceTests
{
    private const string DbContextName = "GetAttributes";
    
    private readonly Table mssqlTable;
    private readonly Table postgresTable;
    
    protected AttributeServiceTests()
    {
        using var dbContext = DatabaseInfoInMemoryDbContext.Create(DbContextName);
        
        var mssqlLanguage = new Language { Name = "MSSQL", ShortName = "mssql", BaseConnectionString = "" };
        var postgresLanguage = new Language { Name = "Postgres", ShortName = "postgres", BaseConnectionString = "" };
        
        var mssqlDatabase = new Database { Name = "MSSQL Database", LanguageId = mssqlLanguage.Id, ConnectionString = "" }.WithLanguage(mssqlLanguage);
        var postgresDatabase = new Database { Name = "Postgres Database", LanguageId = postgresLanguage.Id, ConnectionString = "" }.WithLanguage(postgresLanguage);
        
        mssqlTable = new Table { Name = "MSSQL Table", DatabaseId = mssqlDatabase.Id }.WithDatabase(mssqlDatabase);
        postgresTable = new Table { Name = "Postgres Table", DatabaseId = postgresDatabase.Id }.WithDatabase(postgresDatabase);
        
        mssqlTable.AddAttribute(new Attribute
        {
            Name = "Id",
            Type = "int",
            IsPrimaryKey = true,
            IsNotNull = true,
            IsUnique = false,
            Order = 1,
            TableId = mssqlTable.Id
        });
        
        mssqlTable.AddAttribute(new Attribute
        {
            Name = "Name",
            Type = "nvarchar",
            IsPrimaryKey = false,
            IsNotNull = true,
            IsUnique = false,
            Order = 2,
            TableId = mssqlTable.Id
        });
        
        mssqlTable.AddAttribute(new Attribute
        {
            Name = "Date",
            Type = "datetime",
            IsPrimaryKey = false,
            IsNotNull = true,
            IsUnique = false,
            Order = 3,
            TableId = mssqlTable.Id
        });
        
        postgresTable.AddAttribute(new Attribute
        {
            Name = "Id",
            Type = "int",
            IsPrimaryKey = true,
            IsNotNull = true,
            IsUnique = false,
            Order = 1,
            TableId = postgresTable.Id
        });
        
        postgresTable.AddAttribute(new Attribute
        {
            Name = "Name",
            Type = "varchar",
            IsPrimaryKey = false,
            IsNotNull = true,
            IsUnique = false,
            Order = 2,
            TableId = postgresTable.Id
        });
        
        postgresTable.AddAttribute(new Attribute
        {
            Name = "Date",
            Type = "timestamp",
            IsPrimaryKey = false,
            IsNotNull = true,
            IsUnique = false,
            Order = 3,
            TableId = postgresTable.Id
        });
        
        dbContext.Tables.AddRange(mssqlTable, postgresTable);
        dbContext.SaveChanges();
    }
    
    public class GetByTableTests : AttributeServiceTests
    {
        private static readonly Mock<ILogger<GetAttributesByTableRequestHandler>> LoggerMock = new();
        private readonly GetAttributesByTableRequestHandler sut = new(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
        
        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task GetByTable_ReturnGetAttributesResponse(string language)
        {
            // Arrange
            var table = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), language, "Invalid language")
            };
            var getByTableRequest = new GetAttributesByTableRequest(table.Id);
            
            // Act
            var result = await sut.HandleAsync(getByTableRequest);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeNullOrEmpty();
            result.Value.Should().HaveCount(3);
            result.Value.Should().BeEquivalentTo(table.Attributes);
        }
    }

    public class GetPrimaryKeysByDatabaseTests : AttributeServiceTests
    {
        private static readonly Mock<ILogger<GetPrimaryKeysByDatabaseRequestHandler>> LoggerMock = new();
        private readonly GetPrimaryKeysByDatabaseRequestHandler sut = new(DatabaseInfoInMemoryDbContext.Create(DbContextName), LoggerMock.Object);
        
        [Theory]
        [InlineData("MSSQL")]
        [InlineData("Postgres")]
        public async Task GetPrimaryKeysByDatabase_ReturnGetAttributesResponse(string language)
        {
            // Arrange
            var table = language switch
            {
                "MSSQL" => mssqlTable,
                "Postgres" => postgresTable,
                _ => throw new ArgumentOutOfRangeException(nameof(language), language, "Invalid language")
            };
            var getByDatabaseRequest = new GetPrimaryKeysByDatabaseRequest(table.DatabaseId);
            
            // Act
            var result = await sut.HandleAsync(getByDatabaseRequest);
            
            // Assert
            result.IsSucceeded.Should().BeTrue(string.Join(", ", result.Problems));
            result.Value.Should().NotBeNullOrEmpty();
            var expected = table.Attributes.Where(a => a.IsPrimaryKey).ToList();
            result.Value.Should().BeEquivalentTo(expected);
        }
    }
}